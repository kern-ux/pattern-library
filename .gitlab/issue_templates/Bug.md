## Zusammenfassung

(Fasse den aufgetretenen Fehler kurz zusammen)

## Verwendete KERN-UX Version

(Welche Version wurde verwendet?)

## Schritte zur Reproduktion

(Wie man das Problem reproduzieren kann - das ist sehr wichtig)

## Beispielprojekt

(Falls möglich, erstelle auf [Stackblitz](https://stackblitz.com/) oder [Codesandbox](https://codesandbox.io/) ein Beispielprojekt, das das problematische Verhalten zeigt, und verlinke es hier im Fehlerbericht.

## Aktuelles Fehlverhalten

(Was tatsächlich passiert)

## Erwartetes korrektes Verhalten

(Was du stattdessen sehen solltest)

## Relevante Logs und/oder Screenshots

(Füge relevante Logs ein - verwende Codeblöcke (```) um Konsolenausgaben, Logs und Code zu formatieren, da sie sonst sehr schwer zu lesen sind.)

## Mögliche Lösungen

(Wenn du kannst, verlinke auf die Codezeile, die für das Problem verantwortlich sein könnte)

/label ~Development
/cc @marienfeldtom @DarkoP