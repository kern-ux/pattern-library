import { watchAndRun } from 'vite-plugin-watch-and-run'

const config = {
	plugins: [
		watchAndRun([
			{
				name: 'gen',
				quiet: false,
				watchKind: ['add', 'change', 'unlink'],
				watch: "/**/*.(scss|css)",
				watchFile: undefined,
				run: 'pnpm build',
				delay: 500
			}
		])
	]
}
export default config
