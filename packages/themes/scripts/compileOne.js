import { main as sassCompiler } from "./sassCompiler.js";

const themeName = process.argv.slice(2);
await sassCompiler(themeName);
