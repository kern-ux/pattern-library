import { Theme } from 'adopted-style-sheets';
import { KeyEnum } from './i18n-keys';
import { TagEnum } from './tag-names';
export const Kern = new Theme<'kern', keyof typeof KeyEnum, keyof typeof TagEnum>('kern', KeyEnum, TagEnum);
