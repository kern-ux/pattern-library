import Accordion from "./samples/accordion/Accordion";
import Alert from "./samples/alert/Alert";
import Badge from './samples/badge/Badge';
import Button from "./samples/button/Button";
import Divider from "./samples/divider/Divider";
import Heading from "./samples/heading/Heading";
import Icon from "./samples/icon/Icon";
import InputText from "./samples/input-text/InputText";
import InputFile from "./samples/input-file/InputFile";
import InputEmail from "./samples/input-email/InputEmail";
import InputPassword from "./samples/input-password/InputPassword";
import FormExample from "./samples/form/Form";
import InputDate from "./samples/input-date/InputDate";
import InputNumber from "./samples/input-number/InputNumber";
import InputCheckbox from "./samples/input-checkbox/InputCheckbox";
import InputRadio from "./samples/input-radio/InputRadio";
import Link from "./samples/link/Link";
import LinkButton from "./samples/link-button/Link-Button";
import Loader from "./samples/loader/Loader";
import Progress from "./samples/progress/Progress";
import Select from "./samples/select/Select";
import Text from "./samples/text/Text";
import Kopfzeile from './samples/kopfzeile/Kopfzeile';
import Textarea from './samples/textarea/Textarea';
import Tasklist from './samples/tasklist/Tasklist';
import Dialog from './samples/dialog/Dialog';
function App() {

	return (
		<>
			<Kopfzeile />
			<Accordion />
			<Alert />
			<Badge />
			<Button />
			<Divider />
			<FormExample />
			<Heading />
			<Icon />
			<InputText />
			<InputEmail />
			<InputPassword />
			<InputDate />
			<InputNumber />
			<InputCheckbox />
			<InputRadio />
			<Link />
			<LinkButton />
			<Loader />
			<Progress />
			<Select />
			<Text />
			<Textarea />
			<InputFile />
			<Tasklist />
			<Dialog />
		</>
	)
}

export default App;
