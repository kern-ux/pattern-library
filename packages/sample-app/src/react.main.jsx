import { register } from '@public-ui/components';
import { defineCustomElements as KoliBri } from '@public-ui/components/dist/loader';
import { defineCustomElements as Kern } from '@kern-ux/components/dist/loader';
import {EFA, KERN} from '@kern-ux/themes';

import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';

register([EFA, KERN], [KoliBri, Kern], {
	theme: {
		detect: 'auto',
	}
})
	.then(() => {
		const htmlElement = document.querySelector('section#components-comparison');
		if (htmlElement instanceof HTMLElement) {
			const root = createRoot(htmlElement);
			root.render(
				<StrictMode>
					<App />
				</StrictMode>,
			);
		}
	})
	.catch(console.error);
