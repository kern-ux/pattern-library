import { KernHeading, KernAlert, KernBadge } from '@kern-ux/react';
import React from 'react';

class Heading extends React.Component {
	render() {
		return (
			<div className="component-section" id="heading">
				<header className="section-header">
					<KernHeading _level="2" _label="Headings H1 - H6"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<KernAlert _type="info" _variant="card" _label="Hinweis" _level="4">
					Headings sind Responsive und passen sich der jeweiligen Auflösung an! Die Vorgaben aus der Figma sind Ausgangsgrößen und gelten bei Auflösungen großen
					&gt;1500px. Bei Auflösungen &lt;1500px, werden die Headings-Größen automatisch nach unten skaliert!
				</KernAlert>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Heading Level 1-6"></KernHeading>
					<KernHeading _label="Heading H1" _level="1"></KernHeading>
					<KernHeading _label="Heading H2" _level="2"></KernHeading>
					<KernHeading _label="Heading H3" _level="3"></KernHeading>
					<KernHeading _label="Heading H4" _level="4"></KernHeading>
					<KernHeading _label="Heading H5" _level="5"></KernHeading>
					<KernHeading _label="Heading H6" _level="6"></KernHeading>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Default"></KernHeading>
					<KernHeading _label="Default Heading is H1"></KernHeading>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Secondary Heading"></KernHeading>
					<div className="component-row">
						<KernHeading _label="Heading H1" _secondaryHeadline="Secondary Heading"></KernHeading>
					</div>
				</div>
			</div>
		);
	}
}
export default Heading;
