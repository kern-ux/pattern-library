import { KernLink, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Link extends React.Component {
	render() {
		return (
			<div className="component-section" id="link">
				<header className="section-header">
					<KernHeading _level="2" _label="Link"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Interne Link"></KernHeading>
					<div className="component-row">
						<div>
							<p>
								In diesem Absatz wird ein Link gesetzt, der keine weiteren Attribute enthält.
								<KernLink _href="https://www.w3.org" _label="https://www.w3.org"> 
									{' '}
									Hier steht ein Link
								</KernLink>{' '}
								Er wird standardmäßig als
								<i>
									<b> inline-Element </b>
								</i>
								ausgegeben.
							</p>
							<div>
								<KernLink _href="https://www.dataport.de" _label="weiter zur Dataport" _icons="material-symbols-outlined home"></KernLink>
							</div>
							<div>
								<KernLink _href="/" _label="weiter zur Homepage" _icons="material-symbols-outlined arrow_forward"></KernLink>
							</div>
							<div>
								<KernLink _href="https://google.de" _label="Google Homepage" _icons="material-symbols-outlined home"></KernLink>
							</div>
							<div>
								<KernLink _href="https://google.de" _label="Google Homepage ohne Icon"></KernLink>
							</div>

							<div>
								<KernLink _href="/" _label="Linktext der über mehrere Zeilen geht mit einem Icon" _icons="material-symbols-outlined arrow_forward"></KernLink>
							</div>
							<div>
								<KernLink _href="/" _label="Linktext der über mehrere Zeilen geht auch ohen Icon" _icons="material-symbols-outlined arrow_forward"></KernLink>
							</div>

							<div>
								<KernLink 
									_href="/" 
									_label="Linktext der über mehrere Zeilen geht auch ohen Icon" 
									_icons="material-symbols-outlined arrow_forward"
									_disabled
									>
								</KernLink>
							</div>
							<div>
								<KernLink 
									_href="/" 
									_label="Linktext der über mehrere Zeilen geht auch ohen Icon" 
									_icons="material-symbols-outlined arrow_forward"
									_hideLabel	
									>
								</KernLink>
							</div>
						</div>
					</div>
					<KernHeading className="variant-label" _level="3" _label="Externe Link"></KernHeading>
					<div className="component-row">
						<div>
							<p>
								In diesem Absatz wird ein Link gesetzt, der keine weiteren Attribute enthält.
								<KernLink _href="https://www.w3.org" _label="https://www.w3.org" _target="_blank" _icons="material-symbols-rounded open_in_new">
									{' '}
									Hier steht ein Link
								</KernLink>{' '}
								Er wird standardmäßig als
								<i>
									<b> inline-Element </b>
								</i>
								ausgegeben.
							</p>
							<div>
								<KernLink _href="https://www.dataport.de" _label="Homepage" _target="_blank" _icons="material-symbols-rounded open_in_new"></KernLink>
							</div>
							<div>
								<KernLink _href="/" _label="weiter zur Dataport" _target="_blank" _icons="material-symbols-rounded open_in_new"></KernLink>
							</div>
							<div>
								<KernLink _href="https://google.de" _label="Google Homepage" _target="_blank" _icons="material-symbols-rounded open_in_new"></KernLink>
							</div>
							<div>
								<KernLink _href="htrtps://google.de" _label="Google Homepage ohne Icon" _icons="material-symbols-rounded open_in_new"></KernLink>
							</div>
							<div>
								<KernLink
									_href="/"
									_label="Linktext der über mehrere Zeilen geht mit einem Icon"
									_target="_blank"
									_icons="material-symbols-rounded open_in_new"
								></KernLink>
							</div>
							<div>
								<KernLink
									_href="/"
									_label="Linktext der über mehrere Zeilen geht auch ohen Icon"
									_target="_blank"
									_icons="material-symbols-rounded open_in_new"
								></KernLink>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default Link;
