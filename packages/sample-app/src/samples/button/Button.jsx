import { KernButton, KernAlert, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

const PROPS = {
	_label: 'Primary',
	_variant: 'primary',
};

class Button extends React.Component {
	render() {
		return (
			<div className="component-section" id="button">
				<header className="section-header">
					<KernHeading _level="2" _label="Button"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="React Adapter" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<KernAlert _type="info" _variant="card" _label="Hinweis" _level="4">
					Laut Figma besitzen die Buttons den Status: Selected, bei den Buttons mach dieser Zustand kein Sinn, aber eventuell später bei Button Listen.
				</KernAlert>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Default"></KernHeading>
					<div className="component-row">
						<KernButton {...PROPS}></KernButton>
						<KernButton _label="Secondary" _variant="secondary"></KernButton>
						<KernButton _label="Tertiary" _variant="tertiary"></KernButton>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="With Icon"></KernHeading>
					<div className="component-row">
						<KernButton {...PROPS} _icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_forward' } }"></KernButton>
						<KernButton _label="Secondary" _variant="secondary" _icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_back' } }"></KernButton>
						<KernButton _label="Tertiary" _variant="tertiary" _icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_forward' } }"></KernButton>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Icon Only"></KernHeading>
					<div className="component-row">
						<KernButton _hide-label="true" {...PROPS} _icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_forward' } }"></KernButton>
						<KernButton
							_hide-label="true"
							_label="Secondary"
							_variant="secondary"
							_icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_back' } }"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="Tertiary"
							_variant="tertiary"
							_icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_forward' } }"
						></KernButton>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Disabled"></KernHeading>
					<div className="component-row">
						<KernButton {...PROPS} _disabled></KernButton>
						<KernButton _label="Secondary" _variant="secondary" _disabled></KernButton>
						<KernButton _label="Tertiary" _variant="tertiary" _disabled></KernButton>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Block Button"></KernHeading>

					<KernButton _customClass="block" {...PROPS}></KernButton>
					<KernButton _customClass="block" _label="Secondary" _variant="secondary"></KernButton>
					<KernButton _customClass="block" _label="Tertiary" _variant="tertiary"></KernButton>
					<KernButton _customClass="block" _label="Disabled" _variant="primary" _disabled></KernButton>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Block Button with Icon"></KernHeading>

					<KernButton _customClass="block" {...PROPS} _icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_back' } }"></KernButton>
					<KernButton
						_customClass="block"
						_label="Secondary"
						_variant="secondary"
						_icons="{ 'right': { 'icon': 'material-symbols-outlined arrow_forward' } }"
					></KernButton>
					<KernButton
						_customClass="block"
						_label="Tertiary"
						_variant="tertiary"
						_icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_back' } }"
					></KernButton>
					<KernButton
						_customClass="block"
						_label="Disabled"
						_variant="primary"
						_disabled
						_icons="{ 'right': { 'icon': 'material-symbols-outlined arrow_forward' } }"
					></KernButton>
				</div>
			</div>
		);
	}
}
export default Button;
