import { KernDivider, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Divider extends React.Component {
	render() {
		return (
			<div className="component-section" id="divider">
				<header className="section-header">
					<KernHeading _level="2" _label="Divider"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="React Adapter" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<div className="component-col">
					<KernDivider></KernDivider>
				</div>
			</div>
		);
	}
}
export default Divider;
