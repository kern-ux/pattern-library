import type { FC } from 'react';
import { KernBadge, KernHeading, KernInputPassword } from '@kern-ux/react';
import React, { useEffect, useRef, useState } from 'react';

export const InputPassword: FC = () => {
	return (
		<div className="component-section" id="input-password">
			<header className="section-header">
				<KernHeading _level="2" _label="Input Password"></KernHeading>
				<div className="header-icons">
					<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
					<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
					<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
				</div>
			</header>

			<div className="component-col">
			<KernInputPassword
					_label="Passwort"
					_hasPasswordToggle={false}
				></KernInputPassword>

				<KernInputPassword
					_placeholder="Mit 'Passwort anzeigen' Button"
					_label="Passwort"
					_hasPasswordToggle={true}
				></KernInputPassword>
			</div>
		</div>
	);
};

export default InputPassword;
