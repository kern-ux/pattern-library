import { KernKopfzeile, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Kopfzeile extends React.Component {
	render() {
		return (
			<div className="component-section" id="kopfzeile">
				<header className="section-header">
					<KernHeading _level="2" _label="Kopfzeile"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="React Adapter" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="Figma: v1.5.1" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<div className="u-stack-2">
					<div className="component-col">
						<KernHeading className="variant-label" _level="3" _label="Deutsch mit Fluid Container"></KernHeading>
					</div>
					<KernKopfzeile _language="german" _fluid></KernKopfzeile>
				</div>

				<div className="u-stack-2">
					<div className="component-col">
						<KernHeading className="variant-label" _level="3" _label="Deutsch ohne Fluid Container"></KernHeading>
					</div>
					<KernKopfzeile _language="german"></KernKopfzeile>
				</div>


				<div className="u-stack-2">
					<div className="component-col">
						<KernHeading className="variant-label" _level="3" _label="Englisch mit Fluid Container"></KernHeading>
					</div>
					<KernKopfzeile _language="english"_fluid></KernKopfzeile>
				</div>

				<div className="u-stack-2">
					<div className="component-col">
						<KernHeading className="variant-label" _level="3" _label="Deutsch mit custom Font"></KernHeading>
					</div>
					<KernKopfzeile _language="german" _fluid style={{ '--kopfzeile-font-family': 'Courier New, monospace' }}></KernKopfzeile>
				</div>

				<div className="u-stack-2">
					<div className="component-col">
						<KernHeading className="variant-label" _level="3" _label="Variante mit Label Override"></KernHeading>
					</div>
					<KernKopfzeile _language="german" _label="ich bin ein Override label" _fluid></KernKopfzeile>
				</div>

		</div>
	)
		;
	}
}

export default Kopfzeile;
