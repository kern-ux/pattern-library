import { KernInputCheckbox, KernHeading, KernBadge, KernAlert, KernFieldset } from '@kern-ux/react';
import React from 'react';

class InputCheckbox extends React.Component {
	render() {
		return (
			<div className="component-section" id="input-checkbox">
				<header className="section-header">
					<KernHeading _level="2" _label="Input Checkbox"></KernHeading>
					<div className="header-icons">
						{/* <KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge> */}
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernHeading _level="3" _label="KERN - Input Checkbox"></KernHeading>
					<KernInputCheckbox _label="Label Text"></KernInputCheckbox>
					<KernInputCheckbox _label="Label Text - Pflichtfeld" _required></KernInputCheckbox>
					<KernInputCheckbox _label="Label Text Checked" _checked="true"></KernInputCheckbox>
					<KernInputCheckbox _label="Label Text Disabled" _disabled></KernInputCheckbox>
					<KernInputCheckbox _label="Label Text Checked Disabled" _checked="true" _disabled></KernInputCheckbox>
					<KernInputCheckbox _label="Label Switch" _variant="switch"></KernInputCheckbox>
					<KernInputCheckbox _label="Label Switch Checked" _variant="switch" _checked="true"></KernInputCheckbox>

					<KernHeading _level="3" _label="KobiBri(KOL) - Input Checkbox"></KernHeading>
					<kol-input-checkbox _label="Label Text"></kol-input-checkbox>
					<kol-input-checkbox _label="Label Text - Pflichtfeld" _required></kol-input-checkbox>
					<kol-input-checkbox _label="Label Text Checked" _checked="true"></kol-input-checkbox>
					<kol-input-checkbox _label="Label Text Disabled" _disabled></kol-input-checkbox>
					<kol-input-checkbox _label="Label Text Checked Disabled" _checked="true" _disabled></kol-input-checkbox>
					<kol-input-checkbox _label="Label Switch" _variant="switch"></kol-input-checkbox>
					<kol-input-checkbox _label="Label Switch Checked" _variant="switch" _checked="true"></kol-input-checkbox>

					<KernHeading _level="3" _label="Input Checkbox mit error"></KernHeading>
					<KernInputCheckbox _label="Label Text mit Error" _error="Fehler Text" _required></KernInputCheckbox>
					<KernInputCheckbox _label="Label Text mit Hint" _error="Fehler Text" _hint="Hint Text" _required></KernInputCheckbox>

					<KernHeading _level="3" _label="Input Checkbox mit error in Fieldset"></KernHeading>
					<KernFieldset _legend="Input Checkbox mit error in Fieldsets">
						<KernInputCheckbox _label="Label Text mit Error" _error="Fehler Text" _required></KernInputCheckbox>
						<KernInputCheckbox _label="Label Text mit Hint" _error="Fehler Text" _hint="Hint Text" _required></KernInputCheckbox>
					</KernFieldset>

					<KernHeading _level="3" _label="Einzelne Input Checkbox im Fieldset mit Legende"></KernHeading>
					<KernFieldset _legend="Legend">
						<KernInputCheckbox _label="Label Text"></KernInputCheckbox>
					</KernFieldset>

					<KernHeading _level="3" _label="Einzelne Input Checkbox im Fieldset mit Legende und Hint"></KernHeading>
					<KernFieldset _legend="Legend">
						<KernInputCheckbox _label="Label Text" _hint="Hint Text"></KernInputCheckbox>
					</KernFieldset>

					<KernFieldset _legend="Ich bin die Legende eines Fieldsets">
						<KernInputCheckbox _label="Label Text"></KernInputCheckbox>
						<KernInputCheckbox _label="Label Text Checked" _checked="true"></KernInputCheckbox>
						<KernInputCheckbox _label="Label Text Disabled" _disabled></KernInputCheckbox>
						<KernInputCheckbox _label="Label Text Checked Disabled" _checked="true" _disabled></KernInputCheckbox>
						<KernInputCheckbox _label="Label Switch" _variant="switch"></KernInputCheckbox>
						<KernInputCheckbox _label="Label Switch Checked" _variant="switch" _checked="true"></KernInputCheckbox>
					</KernFieldset>

					<KernFieldset _legend="Ich bin die Legende eines Fieldsets, mit KOL inputs">
						<kol-input-checkbox _label="Label Text"></kol-input-checkbox>
						<kol-input-checkbox _label="Label Text Checked" _checked="true"></kol-input-checkbox>
						<kol-input-checkbox _label="Label Text Disabled" _disabled></kol-input-checkbox>
						<kol-input-checkbox _label="Label Text Checked Disabled" _checked="true" _disabled></kol-input-checkbox>
						<kol-input-checkbox _label="Label Switch" _variant="switch"></kol-input-checkbox>
						<kol-input-checkbox _label="Label Switch Checked" _variant="switch" _checked="true"></kol-input-checkbox>
					</KernFieldset>
				</div>
			</div>
		);
	}
}
export default InputCheckbox;
