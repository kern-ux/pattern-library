import { KernInputRadio, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class InputRadio extends React.Component {
	render() {
		return (
			<div className="component-section" id="input-radio">
				<header className="section-header">
					<KernHeading _level="2" _label="Input Radio"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						{/* <KernBadge _label="Icon color" _status="warning" _icons="material-symbols-rounded filled small badge_warning"></KernBadge> */}
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernInputRadio
						_label="Anrede"
						_value=""
						_id="anrede1"
						_name="anrede1"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'
						_on={{ onChange: (_event, value) => setValue(value) }}>
					</KernInputRadio>
					<KernInputRadio
						_label="Anrede Pflichfeld"
						_required
						_error="Ich bin eine Fehlermeldung!"
						_id="anrede1"
						_name="anrede1"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'>
					</KernInputRadio>
					<KernInputRadio
						_label="Disabled"
						_id="anrede2"
						_disabled
						_name="anrede2"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'>
					</KernInputRadio>
					<KernInputRadio
						_label="Horizontal"
						_id="anrede3"
						_name="anrede3"
						_orientation="horizontal"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'>
					</KernInputRadio>
					<KernInputRadio
						_label="Anrede Error"
						_id="anrede4"
						_name="anrede4"
						_required
						_touched="true"
						_error="Das ist ein Fehler"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'>
					</KernInputRadio>
					<KernInputRadio
						_label="Horizontal Error"
						_id="anrede5"
						_name="anrede5"
						_required
						_touched="true"
						_orientation="horizontal"
						_error="Das ist ein Fehler"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'>
					</KernInputRadio>
					<KernInputRadio
						_label="Radio mit Hint"
						_id="anrede6"
						_name="anrede6"
						_hint="Hint Text"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'>
					</KernInputRadio>
					<KernInputRadio
						_label="Horizontal mit Hint"
						_id="anrede7"
						_name="anrede7"
						_hint="Hint Text"
						_orientation="horizontal"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'>
					</KernInputRadio>
					<KernInputRadio
						_label="Anrede Error"
						_id="anrede8"
						_name="anrede8"
						_required
						_hint="Hint Text"
						_touched="true"
						_error="Das ist ein Fehler"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'>
					</KernInputRadio>
					<KernInputRadio
						_label="Horizontal Error"
						_id="anrede9"
						_name="anrede9"
						_required
						_hint="Hint Text"
						_touched="true"
						_orientation="horizontal"
						_error="Das ist ein Fehler"
						_options='[{"label":"Herr","value":"Herr"},{"label":"Frau","value":"Frau"}, {"label":"Firma","value":"Firma"}]'>
					</KernInputRadio>
				</div>
			</div>
		);
	}
}
export default InputRadio;
