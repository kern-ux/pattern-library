import { KernInputNumber, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class InputNumber extends React.Component {
	render() {
		return (
			<div className="component-section" id="input-number">
				<header className="section-header">
					<KernHeading _level="2" _label="Input Number"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						{/* <KernBadge _label="Icon color" _status="warning" _icons="material-symbols-rounded filled small badge_warning"></KernBadge> */}
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernInputNumber  _label="Number" _icons="material-symbols-outlined filled home"></KernInputNumber>
					<KernInputNumber  _label="Number Hint" _hint="hint Text"></KernInputNumber>
					<KernInputNumber  _label="Number Hint Disabled" _hint="hint Text" _disabled></KernInputNumber>
					<KernInputNumber  _label="Number Hint Readonly" _hint="hint Text" _readOnly></KernInputNumber>
				</div>
			</div>
		);
	}
}
export default InputNumber;