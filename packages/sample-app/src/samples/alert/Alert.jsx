import { KernAlert, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

const PROPS = {
	_type: 'error',
	_label: 'Fehler',
	_level: '3',
};

class Alert extends React.Component {
	render() {
		return (
			<div className="component-section" id="alert">
				<header className="section-header">
					<KernHeading _level="2" _label="Alert"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="TODO: Link Variante" _status="danger" _icons="material-symbols-outlined filled small badge_emergency_home"></KernBadge>

							<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Alerts"></KernHeading>
					<KernAlert _type="info" _variant="msg" _label="Hinweis" _level="4"></KernAlert>
					<KernAlert _type="success" _variant="msg" _label="Erfolg" _level="5"></KernAlert>
					<KernAlert _type="warning" _variant="msg" _label="Warnung" _level="6">
						{' '}
					</KernAlert>
					<KernAlert {...PROPS} _variant="msg"></KernAlert>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Alert Carts"></KernHeading>
					<KernAlert _type="info" _variant="card" _label="Hinweis" _level="4">
						Hier wird der Hinweis näher beschrieben.
					</KernAlert>
					<KernAlert _type="success" _variant="card" _label="Erfolg" _level="5">
						Hier wird der Erfolg näher beschrieben.
					</KernAlert>
					<KernAlert _type="warning" _variant="card" _label="Warnung" _level="6">
						Hier wird die Warnung näher beschrieben.
					</KernAlert>
					<KernAlert {...PROPS} _variant="card">
						Hier wird der Fehler näher beschrieben.
					</KernAlert>
					<KernAlert {...PROPS} _label="Alert mit HTML Content" _variant="card">
						<ul>
							<li>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</li>
							<li>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</li>
							<li>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</li>
						</ul>
					</KernAlert>
				</div>
			</div>
		);
	}
}

export default Alert;
