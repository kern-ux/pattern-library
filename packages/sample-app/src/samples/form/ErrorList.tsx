import { KernAlert, KernLink } from "@kern-ux/react";
import React from "react";
import { useMemo } from "react";

type ErrorListPropType = {
	errors: Record<string, string>;
};

export function ErrorList({ errors }: ErrorListPropType) {
	const links = useMemo(() => {
		return Object.entries(errors).map(([name, error]) => ({
			_href: `#field-${name}`,
			_label: error,
			_name: name,
			_on: {
				onClick: () => {
					const field = document.getElementById(`field-${name}`);
					if (field) {
						field.focus();
					}
				},
			},
		}));
	}, [errors]);

	return (
		<KernAlert _label="Bitte überprüfen Sie Ihre Eingaben" _type="error" _variant="card">
			<ul>
				{links.map((link, index) => (
					<li key={index}>
						<strong>{link._name}: </strong><a href={link._href} onClick={link._on.onClick}>{link._label}</a>
					</li>
				))}
			</ul>
		</KernAlert>
	);
}
