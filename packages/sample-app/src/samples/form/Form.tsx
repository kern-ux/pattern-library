import { InputTypeOnDefault, Iso8601 } from '@public-ui/components';
import { KernBadge, KernButton, KernDivider, KernHeading, KernInputDate, KernInputEmail, KernInputNumber, KernInputText } from '@kern-ux/react';
import { Formik, useFormikContext } from 'formik';
import React, { useEffect, useMemo, useState } from 'react';
import * as Yup from 'yup';
import { ErrorList } from './ErrorList';

type FormValues = {
	date: Iso8601 | null;
	time: Iso8601 | null;
	numberOfPersons: number;
	name: string;
	phone: string;
	email: string;
};

const initialValues: FormValues = {
	date: '2021-10-10',
	time: '10:10',
	numberOfPersons: 2,
	name: '',
	phone: '',
	email: ''
};

const terminSchema = {
	date: Yup.string().required('Bitte Datum wählen.'),
	time: Yup.string().required('Bitte Zeit wählen.'),
	numberOfPersons: Yup.number()
		.required('Bitte Personenanzahl wählen.')
		.max(8, 'Maximal 8 Personen.')

};

const contactSchema = {
	name: Yup.string().required('Bitte Nachname eingeben.'),
	phone: Yup.string().required('Bitte Telefon eingeben.'),
	email: Yup.string().required('Bitte E-Mail Adresse eingeben.')
		.email('Bitte eine gültige E-Mail Adresse eingeben.')
};

function Form() {
	const form = useFormikContext<FormValues>();
	const [errorList, setErrorList] = useState({
		...form.errors
	});

	const showErrorList = useMemo(
		() => Object.keys(errorList).length > 0,
		[errorList]
	);

	useEffect(() => {
		if (errorList) {
			const errorList = document.getElementById('error-list');
			if (errorList) {
				errorList.focus();
			}
		}
	}, [errorList]);

	const createOnChange = (name: keyof FormValues): InputTypeOnDefault => {
		return {
			onChange: (event: Event, value: unknown) => {
				if (event.target) {
					void form.setFieldValue(name, value, true);
				}
			}
		};
	};

	// Hack to trigger validation on mount
	useEffect(() => {
		form.handleSubmit();
	}, []);

	const onSubmit = (event: React.FormEvent) => {
		event.preventDefault(); // Prevent form from submitting in the traditional way
		console.log(form.errors);
		form.handleSubmit();
		setErrorList({
			...form.errors
		});
	};

	return (
		<form
			onSubmit={onSubmit}
			onReset={() => {
				setErrorList({});
				form.handleReset();
				form.handleSubmit(); // Hack to trigger validation on reset
			}}
		>
			{showErrorList && (
				<div className="component-col">
					<div tabIndex={0} id="error-list">
						<ErrorList errors={errorList} />
					</div>
				</div>
			)}
			<div className="component-col">
				<KernInputDate
					_id="field-date"
					_label="Datum"
					_required
					_error={form.errors.date ?? ''}
					_on={createOnChange('date')}
					_touched={showErrorList}
					_value={form.values.date}
				/>
				<KernInputDate
					_id="field-time"
					_label="Uhrzeit"
					_required
					_type="time"
					_error={form.errors.time ?? ''}
					_on={createOnChange('time')}
					_touched={showErrorList}
					_value={form.values.time}
				/>
				<KernInputNumber
					_id="field-numberOfPersons"
					_label="Anzahl Personen"
					_min={1}
					_required
					_error={form.errors.numberOfPersons ?? ''}
					_on={createOnChange('numberOfPersons')}
					_touched={showErrorList}
					_value={form.values.numberOfPersons}
				/>
				<KernInputText
					_id="field-name"
					_label="Name"
					_required
					_error={form.errors.name ?? ''}
					_on={createOnChange('name')}
					_touched={showErrorList}
					_value={form.values.name}
				/>
				<KernInputText
					_id="field-phone"
					_label="Telefon"
					_required
					_type="tel"
					_error={form.errors.phone ?? ''}
					_on={createOnChange('phone')}
					_touched={showErrorList}
					_value={form.values.phone}
				/>
				<KernInputEmail
					_id="field-email"
					_label="E-Mail Adresse"
					_required
					_type="email"
					_error={form.errors.email ?? ''}
					_on={createOnChange('email')}
					_touched={showErrorList}
					_value={form.values.email}
				/>
			</div>
			<KernDivider />

			<div className="component-col">
				<div className="component-row">
					<KernButton
						_label="Anfrage absenden"
						_variant="primary"
						_type="submit"
					/>
					<KernButton _label="Zurücksetzen" _type="reset" _variant="secondary" />
				</div>
			</div>
		</form>
	);
}

const validationSchema = Yup.object().shape({
	...terminSchema,
	...contactSchema
});

function FormExample() {
	const [formValues, setFormValues] = useState(initialValues);

	const onSubmit = (values: FormValues) => {
		console.log('Valid form to submit', values);
		setFormValues(initialValues); // Reset form values after submission
	};

	return (
		<div className="component-section" id="form">
			<header className="section-header">
				<KernHeading _level={2} _label="Form Example"></KernHeading>
				<div className="header-icons">
					<div className="component-row">
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Last update: 28..03.24" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</div>
			</header>
			<div className="component-col">
				<KernHeading _label="Mit Validierung" _level={3} />
			</div>
			<Formik<FormValues>
				initialValues={formValues}
				onSubmit={onSubmit}
				validationSchema={validationSchema}
			>
				<Form />
			</Formik>
		</div>
	);
}

export default FormExample;
