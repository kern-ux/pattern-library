import { KernTextarea, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Textarea extends React.Component {
	render() {
		return (
			<div className="component-section" id="input-textarea">
				<header className="section-header">
					<KernHeading _level="2" _label="Input Textarea"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v1.5.1" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernTextarea _label="Textarea label"></KernTextarea>
					<KernTextarea _label="Textarea label" _hint="Hint Text"></KernTextarea>
					<KernTextarea _label="Textarea label Pflichtfeld" _required></KernTextarea>
					<KernTextarea _label="Textarea label" _error="Das ist eine Fehlermeldung" _required></KernTextarea>
				</div>
			</div>
		);
	}
}
export default Textarea;
