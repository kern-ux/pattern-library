import { KernAccordion, KernAccordionGroup, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

const PROPS = {
	_type: 'error',
	_label: 'Fehler',
	_level: '3',
};

class Accordion extends React.Component {
	render() {
		return (
			<div className="component-section" id="accordion">
				<header className="section-header">
					<KernHeading _level="2" _label="Accordion"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							{/* <KernBadge _label="WIP" _status="warning" _icons="material-symbols-rounded filled small badge_warning"></KernBadge> */}
							<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="Figma: v1.5.1" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<div className="component-col">
				<KernHeading _level="3" _label="Accordions einzeln"></KernHeading>
					<div>
						<KernAccordion _label="Header" _level="1">
							Text
						</KernAccordion>
						<KernAccordion _label="Header" _level="1">
							Text
						</KernAccordion>
						<KernAccordion _label="Header" _level="1">
							Text
						</KernAccordion>
					</div>
				</div>

				<div className="component-col">
					<KernHeading _level="3" _label="Accordion Group"></KernHeading>
					<KernAccordionGroup>
						<KernAccordion _label="Header" _level="1">
							Text
						</KernAccordion>
						<KernAccordion _label="Header" _level="1">
							Text
						</KernAccordion>
						<KernAccordion _label="Header" _level="1">
							Text
						</KernAccordion>
						<KernAccordion _label="Header" _level="1">
							Text
						</KernAccordion>
						<KernAccordion _label="Header" _level="1">
							Text
						</KernAccordion>
					</KernAccordionGroup>
				</div>

			</div>
		);
	}
}

export default Accordion;
