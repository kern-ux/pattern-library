import { KernBadge, KernHeading } from '@kern-ux/react';
import React from 'react';

const PROPS = {
	_label: 'Success',
	_status: 'success',
};

class Badge extends React.Component {
	render() {
		return (
			<div className="component-section" id="badge">
				<header className="section-header">
					<KernHeading _level="2" _label="Badge"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							{/* <KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge> */}
							<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="TODO: _hide-label prop" _status="danger" _icons="material-symbols-outlined filled small badge_emergency_home"></KernBadge>
							<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Normal"></KernHeading>
					<div className="component-row">
						<KernBadge _label="Info" _status="info"></KernBadge>
						<KernBadge></KernBadge>
						<KernBadge _label="Default"></KernBadge>
						<KernBadge {...PROPS}></KernBadge>
						<KernBadge _label="Warning" _status="warning"></KernBadge>
						<KernBadge _label="Danger" _status="danger"></KernBadge>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="With Icon"></KernHeading>
					<div className="component-row">
						<KernBadge _label="Info" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						<KernBadge _label="Default" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge {...PROPS} _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Warning" _status="warning" _icons="material-symbols-rounded filled small badge_warning"></KernBadge>
						<KernBadge _label="Danger" _status="danger" _icons="material-symbols-outlined filled small badge_emergency_home"></KernBadge>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Icon Only"></KernHeading>
					{/* <div className="component-row">
                        <KernBadge _label="Info" _status="info" _icons="material-symbols-outlined filled info" _hide-label></KernBadge>
                        <KernBadge _label="Default" _icons="material-symbols-outlined filled check_circle" _hide-label></KernBadge>
                        <KernBadge {...PROPS} _icons="material-symbols-outlined filled check_circle" _hide-label></KernBadge>
                        <KernBadge _label="Warning" _status="warning" _icons="material-symbols-rounded filled warning" _hide-label></KernBadge>
                        <KernBadge _label="Danger" _status="danger" _icons="material-symbols-outlined filled emergency_home" _hide-label></KernBadge>
                    </div> */}

					<p>
						Um die Benutzerfreundlichkeit und die Zugänglichkeit zu verbessern, sollten Badges immer im Kontext einer aussagekräftigen Überschrift verwendet
						werden.
					</p>
					<p>
						Stellen Sie sicher, dass jede Gruppe von Badges durch eine Überschrift ergänzt wird, die den gemeinsamen Kontext oder das Thema beschreibt.
						Vermeiden Sie es, Badges ohne solche beschreibenden Überschriften zu präsentieren.
					</p>
				</div>
			</div>
		);
	}
}
export default Badge;
