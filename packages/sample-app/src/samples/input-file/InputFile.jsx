import { KernInputFile, KernHeading, KernBadge, KernDivider } from '@kern-ux/react';
import React from 'react';

class InputFile extends React.Component {
	render() {
		return (
			<div className="component-section" id="input-file">
				<header className="section-header">
					<KernHeading _level="2" _label="Input File"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v1.5.1" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						<KernBadge _label="KERN Props Fehlerhaft" _status="danger" _icon="material-symbols-outlined filled small badge_emergency_home"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernHeading _level="3" _label="KERN Input File"></KernHeading>
				</div>

				<div className="component-col">
					<KernInputFile 
					_label="KERN Upload File" 
					_accept="image/jpeg" 
					_allowed-file-types="jpg" 
					_hint="Erlaubte Formate: jpg, png, pdf"/>
				</div>

				<div className="component-col">
					<KernInputFile 
						_label="KERN Upload File - Error" 
						_accept="image/jpeg" 
						_allowed-file-types="jpg" 
						_hint="Erlaubte Formate: jpg, png, pdf"
						_required
						_error="Das ist ein Pflichtfeld"
					/>
				</div>
				<div className="component-col">
					<KernInputFile 
						_label="KERN Upload File - Error" 
						_accept="image/jpeg" 
						_allowed-file-types="jpg" 
						_hint="Erlaubte Formate: jpg, png, pdf"
						_required
						_msg="Das ist ein Pflichtfeld"
						_type="error"
					/>
				</div>
				<div className="component-col">
					<KernInputFile 
						_label="KERN Upload File - Disabled" 
						_accept="image/jpeg" 
						_allowed-file-types="jpg" 
						_hint="Erlaubte Formate: jpg, png, pdf"
						_disabled
						_required
					/>
				</div>

				<div className="component-col">
					<KernHeading _level="3" _label="KERN Input Multifilrs"></KernHeading>
				</div>	
				<div className="component-col">
					<KernInputFile 
						_label="KERN Upload File - Disabled" 
						_accept="image/jpeg" 
						_allowed-file-types="jpg" 
						_hint="Erlaubte Formate: jpg, png, pdf"
						_disabled
						_required
						_multiple="true"
					/>
				</div>		

				<div className="component-col">
					<KernHeading _level="3" _label="KOL Input File"></KernHeading>
				</div>
				<div className="component-col">
					<kol-input-file 
						_label="KOL Upload File" 
						_accept="image/png" 
						_allowed-file-types="png" 
						_hint="Erlaubte Formate: png"
					/>
				</div>
				<div className="component-col">
					<kol-input-file 
						_label="KOL Upload File - Pflicht" 
						_accept="image/jpeg" 
						_allowed-file-types="jpg" 
						_hint="Erlaubte Formate: jpg"
						_required
					/>
				</div>
				<div className="component-col">
					<kol-input-file 
						_label="KOL Upload File - Disabled" 
						_accept="image/jpeg" 
						_allowed-file-types="jpg" 
						_hint="Erlaubte Formate: jpg"
						_disabled
						_required
					/>
				</div>
				<div className="component-col">
					<kol-input-file 
						_label="KOL Upload File - Error" 
						_accept="image/jpeg" 
						_allowed-file-types="jpg" 
						_hint="Erlaubte Formate: jpg"
						_error="Das ist ein Pflichtfeld"
						_required
					/>
				</div>


				<div className="component-col">
					<KernHeading _level="3" _label="KOL Input Multifiles"></KernHeading>
				</div>	
				<div className="component-col">
					<kol-input-file 
						_label="KERN Upload Multiple files" 
						_accept="image/png, application/pdf" 
						_hint="Erlaubte Formate: png, pdf"
						_required
						_multiple="true"
					/>
				</div>	

				<div className="component-col">
					<KernHeading _level="3" _label="KOL Input File only PNG"></KernHeading>
				</div>	
				<div className="component-col">
					<kol-input-file 
						_label="KERN Upload File only PNG" 
						_accept="image/png" 
						_hint="Erlaubte Formate: png"
						_required
					/>
				</div>	
			</div>
		);
	}
}
export default InputFile;
