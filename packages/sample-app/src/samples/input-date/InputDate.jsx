import { KernInputDate, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class InputDate extends React.Component {
	render() {
		return (
			<div className="component-section" id="input-date">
				<header className="section-header">
					<KernHeading _level="2" _label="Input Date"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Darstellung der Icons in Firefox nicht möglich!" _status="warning" _icons="material-symbols-rounded filled small badge_warning"></KernBadge>
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernInputDate _type="date" _label="Date" _icons="material-symbols-outlined filled home"></KernInputDate>
					<KernInputDate _label="Date" _hint='Hint Text'></KernInputDate>
					<KernInputDate _type="datetime-local" _label="Datetime local" />
					<KernInputDate _type="month" _label="Month" />
					<KernInputDate _type="time" _label="Time" />
					<KernInputDate _type="week" _label="Week" />

					<KernInputDate _type="date" _label="Date" _required _error="Ich bin ein Fehler!" _hint="Hint Text"/>
					<KernInputDate _type="date" _label="Date" _readOnly _value="02.02.2012" />
				</div>
			</div>
		);
	}
}
export default InputDate;