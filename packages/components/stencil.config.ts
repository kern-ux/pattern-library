import { promises as fs } from 'fs';

// import { angularOutputTarget } from '@stencil/angular-output-target';
import { Config } from '@stencil/core';
import { JsonDocs, OutputTarget } from '@stencil/core/internal';
import { postcss } from '@stencil-community/postcss';
import { reactOutputTarget } from '@stencil/react-output-target';

const TAGS = [
	'kern-accordion',
	'kern-accordion-group',
	'kern-button',
	'kern-heading',
	'kern-badge',
	'kern-icon',
	'kern-text',
	'kern-alert',
	'kern-link',
	'kern-divider',
	'kern-dialog',
	'kern-link-group',
	'kern-button-group',
	'kern-input-text',
	'kern-input-radio',
	'kern-input-password',
	'kern-input-checkbox',
	'kern-textarea',
	'kern-input-file',
	'kern-input-date',
	'kern-input-email',
	'kern-select',
	'kern-loader',
	'kern-form',
	'kern-progress',
	'kern-link-button',
	'kern-modal',
	'kern-fieldset',
	'kern-kopfzeile',
];
const EXCLUDE_TAGS = [];
const BUNDLES: {
	components: string[];
}[] = [];
TAGS.forEach((tag) => {
	BUNDLES.push({
		components: [tag],
	});
});

async function generateCustomElementsJson(docsData: JsonDocs) {
	const jsonData = {
		version: require('./package.json').version,
		tags: docsData.components.map((component) => ({
			name: component.tag,
			// path: component.filePath,
			description: component.docs,

			attributes: component.props
				.filter((prop) => prop.attr)
				.map((prop) => ({
					name: prop.attr,
					type: prop.type,
					description: prop.docs,
					defaultValue: prop.default,
					required: prop.required,
				})),

			events: component.events.map((event) => ({
				name: event.event,
				description: event.docs,
			})),

			methods: component.methods.map((method) => ({
				name: method.name,
				description: method.docs,
				signature: method.signature,
			})),

			slots: component.slots.map((slot) => ({
				name: slot.name,
				description: slot.docs,
			})),

			cssProperties: component.styles
				.filter((style) => style.annotation === 'prop')
				.map((style) => ({
					name: style.name,
					description: style.docs,
				})),

			cssParts: component.parts.map((part) => ({
				name: part.name,
				description: part.docs,
			})),
		})),
	};

	await fs.writeFile('./custom-elements.json', JSON.stringify(jsonData, null, 2));
}

let outputTargets: OutputTarget[] = [
	{
		type: 'dist',
		copy: [
			{
				src: 'assets',
			},
		],
	},
	{
		type: 'www',
		serviceWorker: null,
		copy: [
			{
				src: 'assets',
			},
		],
	},
	// {
	//   type: 'custom',
	//   name: 'CSP',
	//   generator: generateCSPHashes,
	// },
];
if (process.env.NODE_ENV === 'production') {
	outputTargets = outputTargets.concat([
		reactOutputTarget({
			componentCorePackage: '@kern-ux/components',
			excludeComponents: EXCLUDE_TAGS,
			proxiesFile: '../adapters/react/src/index.ts',
			includeDefineCustomElements: false,
		}),
		{
			type: 'dist-custom-elements',
			minify: true,
		},
		// {
		// 	type: 'dist-custom-elements-bundle',
		// 	externalRuntime: false,
		// },
		// {
		// 	// https://stenciljs.com/docs/hydrate-app
		// 	type: 'dist-hydrate-script',
		// },
		{
			// https://stenciljs.com/docs/docs-vscode
			type: 'docs-vscode',
			file: 'vscode-custom-data.json',
		},
		{
			type: 'docs-custom',
			generator: generateCustomElementsJson,
		},
		// {
		//   file: 'docs.json',
		//   type: 'docs-json',
		// },
		{
			// dir: 'docs',
			footer: '',
			type: 'docs-readme',
			strict: true,
		},
	]);
}

export const config: Config = {
	// buildEs5: true,
	// https://stenciljs.com/docs/config-extras
	extras: {
		// appendChildSlotFix: true,
		// cloneNodeFix: true,
		enableImportInjection: true,
		// initializeNextTick: true,
		// lifecycleDOMEvents: true,
		// scopedSlotTextContentFix: true,
		// scriptDataOpts: true,
		// slotChildNodesFix: true,
		tagNameTransform: true,
	},
	// enableCache: true,
	invisiblePrehydration: true,
	hashFileNames: false,
	bundles: BUNDLES,
	globalScript: 'src/global/script.ts',
	// globalStyle: 'src/global/style.css',
	namespace: 'kern',
	preamble: 'Web component library based on KoliBri.',
	outputTargets: outputTargets,
	plugins: [postcss()],
	rollupPlugins: {
		before: [],
		after: [],
	},
	taskQueue: 'immediate',
};
