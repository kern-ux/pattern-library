# kern-tasklist-item

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute      | Description                                                                                                        | Type                                                            | Default     |
| -------------------------- | -------------- | ------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------- | ----------- |
| `_badge`                   | `_badge`       | Defines the state of the task.                                                                                     | `"blocked"` \| `"complete"` \| `"empty"` \| `"error"` \| `"incomplete"` | `'empty'`   |
| `_href` _(required)_       | `_href`        | Gibt die Ziel-Url des Links an.                                                                                    | `string`                                                        | `undefined` |
| `_id`                      | `_id`          | Defines the internal ID of the primary component element.                                                          | `string` \| `undefined`                                           | `undefined` |
| `_isEditable` _(required)_ | `_is-editable` | Defines if the task is editable or not.                                                                            | `boolean`                                                       | `undefined` |
| `_label` _(required)_      | `_label`       | Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.). | `string`                                                        | `undefined` |


## Dependencies

### Depends on

- [kern-badge](../badge)
- [kern-link-button](../link-button)
- [kern-text](../text)

### Graph
```mermaid
graph TD;
  kern-tasklist-item --> kern-badge
  kern-tasklist-item --> kern-link-button
  kern-tasklist-item --> kern-text
  style kern-tasklist-item stroke:#333,stroke-width:4px
```

----------------------------------------------


