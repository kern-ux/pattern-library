# kern-badge

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute | Description                                                | Type                                           | Default     |
| --------------------- | --------- | ---------------------------------------------------------- | ---------------------------------------------- | ----------- |
| `_icons` _(required)_ | `_icons`  | Iconklasse (z.B.: "material-symbols-outlined filled info") | `string`                                       | `undefined` |
| `_id`                 | `_id`     | Gibt die ID des Badges an.                                 | `string` \| `undefined`                          | `undefined` |
| `_label` _(required)_ | `_label`  | Gibt den Text des Badges an.                               | `string`                                       | `undefined` |
| `_status`             | `_status` | Gibt den Typ des Badges an.                                | `"danger"` \| `"info"` \| `"success"` \| `"warning"` | `'success'` |


## Dependencies


### Graph
```mermaid
graph TD;
  kern-tasklist-item --> kern-badge
  style kern-badge stroke:#333,stroke-width:4px
```

----------------------------------------------


