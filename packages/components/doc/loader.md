# kern-loader

<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                            | Type                                      | Default   |
| ---------- | ---------- | ------------------------------------------------------ | ----------------------------------------- | --------- |
| `_show`    | `_show`    | Makes the element show up.                             | `boolean` \| `undefined`                    | `false`   |
| `_variant` | `_variant` | Defines which variant should be used for presentation. | `"cycle"` \| `"dot"` \| `"none"` \| `undefined` | `'cycle'` |


----------------------------------------------


