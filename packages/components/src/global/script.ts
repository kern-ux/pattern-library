import { EFA, KERN } from '@kern-ux/themes';
import { setMode } from '@stencil/core';
import { getThemeDetails, register, setThemeStyle } from 'adopted-style-sheets';

register([EFA, KERN], [], {
	theme: {
		detect: 'auto',
	},
}).catch(console.warn);

export default (): void => {
	setMode((elm) => {
		if (elm.shadowRoot instanceof ShadowRoot) {
			setThemeStyle(elm, getThemeDetails(elm));
		}
		return 'default';
	});
};
