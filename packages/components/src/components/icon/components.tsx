import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States, KernIconProp } from './schema';
import { watchString } from '../../utils/prop.validators';
@Component({
	tag: 'kern-icon',
	styleUrls: {
		default: './style.css',
	},
	shadow: false,
})
export class KernIcon implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Setzt die sichtbare oder semantische Beschriftung der Komponente (z.B. Aria-Label, Label, Headline, Caption, Summary usw.).
	 */
	@Prop() public _label!: string;

	/**
	 * Setzt die Iconklasse (z.B.: `_icons="material-symbols-outlined filled home`).
	 */
	@Prop() public _icons!: KernIconProp;

	@State() public state: States = {
		_label: '…', // required
		_icons: 'material-symbols-outlined filled home',
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		watchString(this, '_label', value, {
			required: true,
		});
	}

	@Watch('_icons')
	public validateIcons(value?: string): void {
		watchString(this, '_icons', value, {
			required: true,
		});
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.validateIcons(this._icons);
	}

	public render(): JSX.Element {
		return <kol-icon _label={this.state._label} _icons={this.state._icons}></kol-icon>;
	}
}
