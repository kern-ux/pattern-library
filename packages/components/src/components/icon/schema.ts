import { Generic } from 'adopted-style-sheets';
import { LabelPropType } from '@public-ui/schema';

/** Icon type for any icon Class **/
export type KernIconProp = string;

export type RequiredProps = {
	icons: KernIconProp;
	label: string;
};

export type OptionalProps = NonNullable<unknown>;

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	icons: string;
	label: LabelPropType;
};
export type OptionalStates = OptionalProps;
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
