import { Generic } from 'adopted-style-sheets';
import {
	Stringified,
	LabelWithExpertSlotPropType,
	InputTypeOnDefault,
	IdPropType,
	NamePropType,
	SuggestionsPropType,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
	Orientation,
	OptionsPropType,
	W3CInputValue,
} from '@public-ui/schema';

export type RequiredProps = {
	label: string;
};

export type OptionalProps = {};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelWithExpertSlotPropType;
};

export type OptionalStates = {
	accessKey: string;
	alert: boolean;
	disabled: boolean;
	error: string;
	hideError: boolean;
	hideLabel: boolean;
	hint: string;
	id: IdPropType;
	options: OptionsPropType;
	name: NamePropType;
	on: InputTypeOnDefault;
	orientation: Orientation;
	readOnly: boolean;
	required: boolean;
	suggestions: SuggestionsPropType;
	syncValueBySelector: SyncValueBySelectorPropType;
	tabIndex: number;
	tooltipAlign: TooltipAlignPropType;
	touched: boolean;
	value: Stringified<W3CInputValue>;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
