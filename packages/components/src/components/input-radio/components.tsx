import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, State, Prop, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import {
	ButtonProps,
	IdPropType,
	InputTypeOnDefault,
	LabelWithExpertSlotPropType,
	NamePropType,
	OptionsPropType,
	Orientation,
	Stringified,
	SuggestionsPropType,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
	W3CInputValue,
} from '@public-ui/schema';

@Component({
	tag: 'kern-input-radio',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernInputRadio implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Definiert, welche Tastenkombination verwendet werden kann, um das interaktive Element des Komponenten auszulösen oder zu fokussieren.
	 */
	@Prop() public _accessKey?: string;

	/**
	 * Definiert, ob die Bildschirmleser die Benachrichtigung vorlesen sollen.
	 */
	@Prop({ mutable: true, reflect: true }) public _alert?: boolean = true;

	/**
	 * Macht das Element nicht fokussierbar und ignoriert alle Ereignisse.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Definiert den Text der Fehlermeldung.
	 */
	@Prop() public _error?: string;

	/**
	 * Zeigt die Zeichenanzahl an der unteren Grenze der Eingabe an.
	 */
	@Prop() public _hasCounter?: boolean = false;

	/**
	 * Versteckt die Fehlermeldung, lässt sie aber im DOM für das aria-describedby der Eingabe.
	 */
	@Prop({ mutable: true, reflect: true }) public _hideError?: boolean = false;

	/**
	 * Versteckt die Beschriftung standardmäßig und zeigt den Beschriftungstext mit einem Tooltip an, wenn das interaktive Element fokussiert ist oder die Maus darüber ist.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Definiert den Hinweistext.
	 */
	@Prop() public _hint?: string = '';

	/**
	 * Definiert die interne ID des primären Komponentenelements.
	 */
	@Prop() public _id?: IdPropType;

	/**
	 * Definiert das sichtbare oder semantische Label der Komponente (z.B. aria-label, label, Überschrift, Beschriftung, Zusammenfassung, etc.). Setzen Sie auf `false`, um den Experten-Slot zu aktivieren.
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Options the user can choose from.
	 */
	@Prop() public _options?: OptionsPropType;

	/**
	 * Definiert den technischen Namen eines Eingabefelds.
	 */
	@Prop() public _name?: NamePropType;

	/**
	 * Gibt die EventCallback-Funktionen für das Input-Event an.
	 */
	@Prop() public _on?: InputTypeOnDefault;

	/**
	 * Defines whether the orientation of the component is horizontal or vertical.
	 */
	@Prop() public _orientation?: Orientation = 'vertical';

	/**
	 * Macht das Eingabeelement schreibgeschützt.
	 */
	@Prop() public _readOnly?: boolean = false;

	/**
	 * Macht das Eingabeelement erforderlich.
	 */
	@Prop() public _required?: boolean = false;

	/**
	 * Suggestions to provide for the input.
	 */
	@Prop() public _suggestions?: SuggestionsPropType;

	/**
	 * Allows to add a button with an arbitrary action within the element (_hide-label only).
	 */
	@Prop() public _smartButton?: Stringified<ButtonProps>;

	/**
	 * Selektor zur Synchronisierung des Wertes mit einem anderen Eingabeelement.
	 */
	@Prop() public _syncValueBySelector?: SyncValueBySelectorPropType;

	/**
	 * Definiert, welchen Tab-Index das primäre Element der Komponente hat. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Definiert, wo der Tooltip vorzugsweise angezeigt werden soll: oben, rechts, unten oder links.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'top';

	/**
	 * Zeigt an, ob die Eingabe von einem Benutzer berührt wurde.
	 */
	@Prop({ mutable: true, reflect: true }) public _touched?: boolean = false;

	/**
	 * Defines the value of the input.
	 * @see Known bug: https://github.com/ionic-team/stencil/issues/3902
	 */
	@Prop() public _value?: Stringified<W3CInputValue>;

	// States
	@State() public state: States = {
		_label: '',
		_hideError: false,
		_hideLabel: false,
		_options: [],
		_orientation: 'vertical',
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
	}

	public render(): JSX.Element {
		return (
			<kol-input-radio
				class={{
					radio: true,
					disabled: Boolean(this.state._disabled),
				}}
				_accessKey={this._accessKey}
				_disabled={this._disabled}
				_hideLabel={this._hideLabel}
				_hideError={this._hideError}
				_orientation={this._orientation}
				_name={this._name}
				_on={this._on}
				_readOnly={this._readOnly}
				_suggestions={this._suggestions}
				_syncValueBySelector={this._syncValueBySelector}
				_tabIndex={this._tabIndex}
				_alert={this._alert}
				_error={this._error}
				_hint={this._hint}
				_id={this._id}
				_options={this._options}
				_label={this.state._label}
				_renderNoLabel={true}
				_required={this._required}
				_tooltipAlign={this._tooltipAlign}
				_touched={this._touched}
				_value={this._value}
			></kol-input-radio>
		);
	}
}
