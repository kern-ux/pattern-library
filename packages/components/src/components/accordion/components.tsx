import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { HeadingLevel, KoliBriAccordionCallbacks } from '@public-ui/schema';

@Component({
	tag: 'kern-accordion',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernAccordion implements Generic.Element.ComponentApi<RequiredProps, NonNullable<unknown>, RequiredStates, OptionalStates> {
	/**
	 * Makes the element not focusable and ignore all events.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.).
	 */
	@Prop() public _label!: string;

	/**
	 * Defines which H-level from 1-6 the heading has. 0 specifies no heading and is shown as bold text.
	 */
	@Prop() public _level?: HeadingLevel = 3;

	/**
	 * Gibt die EventCallback-Funktionen an.
	 */
	@Prop() public _on?: KoliBriAccordionCallbacks;

	/**
	 * If set (to true) opens/expands the element, closes if not set (or set to false).
	 * @TODO: Change type back to `OpenPropType` after Stencil#4663 has been resolved.
	 */
	@Prop({ mutable: true, reflect: true }) public _open?: boolean = false;

	@State() public state: States = {
		_label: '',
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		// watchString(this, '_label', value);
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}
	public componentWillLoad(): void {
		this.validateLabel(this._label);
	}

	public render(): JSX.Element {
		return (
			<kol-accordion _label={this.state._label} _level={this._level} _on={this._on} _open={this._open} _disabled={this._disabled}>
				<slot />
			</kol-accordion>
		);
	}
}
