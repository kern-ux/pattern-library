import { Generic } from 'adopted-style-sheets';
import {
	IdPropType,
	InputTypeOnOff,
	Iso8601,
	KoliBriIconsProp,
	LabelWithExpertSlotPropType,
	MsgPropType,
	NamePropType,
	SuggestionsPropType,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';

/** Icon type for any icon Class **/
export type KernIconProp = string;

export type RequiredProps = {
	label: string;
};

export type OptionalProps = {
	icons: KernIconProp;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	id: IdPropType;
	label: LabelWithExpertSlotPropType;
};

export type OptionalStates = {
	accessKey: string;
	autoComplete: InputTypeOnOff;
	disabled: boolean;
	hideErrors: boolean;
	hideLabel: boolean;
	hint: string;
	name: NamePropType;
	error: string;
	value: number | Iso8601 | null;
	min: number | Iso8601;
	max: number | Iso8601;
	step: number;
	icons: KoliBriIconsProp;
	readonly: boolean;
	required: boolean;
	tooltipAlign: TooltipAlignPropType;
	suggestions: SuggestionsPropType;
	placeholder: string;
	syncValueBySelector: SyncValueBySelectorPropType;
	msg: MsgPropType;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
