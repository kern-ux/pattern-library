import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, State, Prop, Watch, Host } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { KernIconProp } from '../button/schema';
import {
	ButtonProps,
	IdPropType,
	InputDateType,
	InputTypeOnDefault,
	InputTypeOnOff,
	Iso8601,
	LabelWithExpertSlotPropType,
	Stringified,
	SuggestionsPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';

@Component({
	tag: 'kern-input-date',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernInputDate implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Definiert, welche Tastenkombination verwendet werden kann, um das interaktive Element des Komponenten auszulösen oder zu fokussieren.
	 */
	@Prop() public _accessKey?: string;

	/**
	 * Legt fest, ob das Eingabefeld automatisch vervollständigt werden kann.
	 */
	@Prop() public _autoComplete?: InputTypeOnOff;

	/**
	 * Macht das Element nicht fokussierbar und ignoriert alle Ereignisse.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Definiert den Text der Fehlermeldung.
	 */
	@Prop() public _error?: string;

	/**
	 * Versteckt die Fehlermeldung, lässt sie aber im DOM für das aria-describedby der Eingabe.
	 */
	@Prop({ mutable: true, reflect: true }) public _hideError?: boolean = false;

	/**
	 * Versteckt die Beschriftung standardmäßig und zeigt den Beschriftungstext mit einem Tooltip an, wenn das interaktive Element fokussiert ist oder die Maus darüber ist.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Definiert den Hinweistext.
	 */
	@Prop() public _hint?: string = '';

	/**
	 * Defines the current length of the input value.
	 */
	@Prop() readonly _currentLength?: number;

	/**
	 * Definiert die interne ID des primären Komponentenelements.
	 */
	@Prop() public _id?: IdPropType;

	/**
	 * Definiert das sichtbare oder semantische Label der Komponente (z.B. aria-label, label, Überschrift, Beschriftung, Zusammenfassung, etc.). Setzen Sie auf `false`, um den Experten-Slot zu aktivieren.
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Defines the largest possible input value.
	 */
	@Prop() public _max?: Iso8601 | Date;

	/**
	 * Defines the smallest possible input value.
	 */
	@Prop() public _min?: Iso8601 | Date;

	/**
	 * Defines the technical name of an input field.
	 */
	@Prop() public _name?: string;

	/**
	 * Gibt die EventCallback-Funktionen für das Input-Event an.
	 */
	@Prop() public _on?: InputTypeOnDefault;

	/**
	 * Setzt die Iconklasse (z.B.: `_icon="material-symbols-outlined home"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Macht das Eingabeelement schreibgeschützt.
	 */
	@Prop() public _readOnly?: boolean = false;

	/**
	 * Macht das Eingabeelement erforderlich.
	 */
	@Prop() public _required?: boolean = false;

	/**
	 * Suggestions to provide for the input.
	 */
	@Prop() public _suggestions?: SuggestionsPropType;

	/**
	 * Definiert, wo der Tooltip vorzugsweise angezeigt werden soll: oben, rechts, unten oder links.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'top';

	/**
	 * Defines the step size for value changes.
	 */
	@Prop() public _step?: number;

	/**
	 * Definiert, welchen Tab-Index das primäre Element der Komponente hat. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Zeigt an, ob die Eingabe von einem Benutzer berührt wurde.
	 */
	@Prop({ mutable: true, reflect: true }) public _touched?: boolean = false;

	/**
	 * Defines the value of the input.
	 */
	@Prop({ mutable: true }) public _value?: Iso8601 | Date | null;

	/**
	 * Defines either the type of the component or of the components interactive element.
	 */
	@Prop() public _type: InputDateType = 'date';

	/**
	 * Allows to add a button with an arbitrary action within the element (_hide-label only).
	 */
	@Prop() public _smartButton?: Stringified<ButtonProps>;

	// States
	@State() public state: States = {
		_label: '', // ⚠ required
		_icons: {},
	};

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	@Watch('_label')
	public validateLabel(value?: LabelWithExpertSlotPropType): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	public componentWillLoad(): void {
		this.validateIcons(this._icons);
		this.validateLabel(this._label);
	}

	public render(): JSX.Element {
		return (
			<Host>
				<kol-input-date
					class={{
						'hide-label': !!this._hideLabel,
					}}
					_autoComplete={this._autoComplete}
					_accessKey={this._accessKey}
					_disabled={this._disabled}
					_error={this._error}
					_hideError={this._hideError}
					_hideLabel={this._hideLabel}
					_hint={this._hint}
					_icons={this.state._icons}
					_id={this._id}
					_label={this.state._label}
					_suggestions={this._suggestions}
					_readOnly={this._readOnly}
					_required={this._required}
					_type={this._type}
					_smartButton={this._smartButton}
					_tooltipAlign={this._tooltipAlign}
					_tabIndex={this._tabIndex}
					_on={this._on}
					_currentLength={this._currentLength}
					_min={this._min}
					_max={this._max}
					_step={this._step}
					_touched={this._touched}
					_name={this._name}
					_value={this._value}
				></kol-input-date>
			</Host>
		);
	}
}
