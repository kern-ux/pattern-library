import { Generic } from 'adopted-style-sheets';
import {
	KoliBriIconsProp,
	Stringified,
	InputTextType,
	LabelWithExpertSlotPropType,
	InputTypeOnDefault,
	InputTypeOnOff,
	IdPropType,
	NamePropType,
	SuggestionsPropType,
	ButtonProps,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';
import { KernIconProp } from '../icon/schema';

export type RequiredProps = {
	label: string;
};

export type OptionalProps = {
	icons: KernIconProp;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelWithExpertSlotPropType;
};

export type OptionalStates = {
	accessKey: string;
	alert: boolean;
	autocomplete: InputTypeOnOff;
	disabled: boolean;
	error: string;
	hasCounter: boolean;
	hideError: boolean;
	hideLabel: boolean;
	hint: string;
	icons: Stringified<KoliBriIconsProp>;
	id: IdPropType;
	maxlength: number;
	name: NamePropType;
	on: InputTypeOnDefault;
	pattern: string;
	placeholder: string;
	readOnly: boolean;
	required: boolean;
	suggestions: SuggestionsPropType;
	smartButton: Stringified<ButtonProps>;
	syncValueBySelector: SyncValueBySelectorPropType;
	tabIndex: number;
	tooltipAlign: TooltipAlignPropType;
	touched: boolean;
	type: InputTextType;
	value: string;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
