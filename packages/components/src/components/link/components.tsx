import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States, KernIconProp } from './schema';
import {
	AccessKeyPropType,
	AlternativeButtonLinkRolePropType,
	AriaCurrentValuePropType,
	DownloadPropType,
	HrefPropType,
	LabelWithExpertSlotPropType,
	LinkOnCallbacksPropType,
	LinkTargetPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';

@Component({
	tag: 'kern-link',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernLink implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Defines the elements access key.
	 */
	@Prop() public _accessKey?: AccessKeyPropType;

	/**
	 * Defines the value for the aria-current attribute.
	 */
	@Prop() public _ariaCurrentValue?: AriaCurrentValuePropType;

	/**
	 * Teilt dem Browser mit, dass sich hinter dem Link eine Datei befindet. Setzt optional den Dateinamen.
	 */
	@Prop() public _download?: DownloadPropType;

	/**
	 * Blendet die Beschriftung (Label) aus und zeigt sie stattdessen mittels eines Tooltips an.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Gibt die Ziel-Url des Links an.
	 */
	@Prop() public _href!: HrefPropType;

	/**
	 * Setzt die sichtbare oder semantische Beschriftung der Komponente (z.B. Aria-Label, Label, Headline, Caption, Summary usw.).
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Setzt die Iconklasse (z.B.: `_icon="material-symbols-outlined home"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Defines the callback functions for links.
	 */
	@Prop() public _on?: LinkOnCallbacksPropType;

	/**
	 * Defines the role of the components primary element.
	 */
	@Prop() public _role?: AlternativeButtonLinkRolePropType;

	/**
	 * Defines which tab-index the primary element of the component has. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Defines where to open the link.
	 */
	@Prop() public _target?: LinkTargetPropType;

	/**
	 * Defines where to show the Tooltip preferably: top, right, bottom or left.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'right';

	@State() public state: States = {
		_href: '…', // ⚠ required
		_icons: {},
		_label: '',
	};

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	@Watch('_href')
	public validateHref(value?: string): void {
		if (typeof value === 'string') {
			this.state._href = value;
		}
	}

	public componentWillLoad(): void {
		this.validateIcons(this._icons);
		this.validateHref(this._href);
	}

	public render(): JSX.Element {
		return (
			<kol-link
				_href={this.state._href}
				_hide-label={this.state._hideLabel}
				_label={this._label || this.state._href}
				_icons={this.state._icons}
				_accessKey={this._accessKey}
				_ariaCurrentValue={this._ariaCurrentValue}
				_download={this._download}
				_hideLabel={this._hideLabel}
				_on={this._on}
				_role={this._role}
				_tabIndex={this._tabIndex}
				_target={this._target}
				_tooltipAlign={this._tooltipAlign}
			></kol-link>
		);
	}
}
