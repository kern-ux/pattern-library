import { Generic } from 'adopted-style-sheets';
import {
	AccessKeyPropType,
	AlternativeButtonLinkRolePropType,
	AriaCurrentValuePropType,
	DownloadPropType,
	HrefPropType,
	KoliBriIconsProp,
	LabelWithExpertSlotPropType,
	LinkOnCallbacksPropType,
	LinkTargetPropType,
	Stringified,
	TooltipAlignPropType,
} from '@public-ui/schema';

/** Icon type for any icon Class **/
export type KernIconProp = string;

/**
 * API
 */
export type RequiredProps = {
	href: string;
};
export type OptionalProps = {
	icons: KernIconProp;
};
export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;
export type KernLinkProps = Generic.Element.Members<RequiredStates, OptionalStates>;

export type RequiredStates = RequiredProps;
export type OptionalStates = {
	label: LabelWithExpertSlotPropType;
	icons: Stringified<KoliBriIconsProp>;
	hideLabel: boolean;
	accessKey: AccessKeyPropType;
	ariaCurrentValue: AriaCurrentValuePropType;
	disabled: boolean;
	download: DownloadPropType;
	href: HrefPropType;
	on: LinkOnCallbacksPropType;
	role: AlternativeButtonLinkRolePropType;
	tabIndex: number;
	target: LinkTargetPropType;
	tooltipAlign: TooltipAlignPropType;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
