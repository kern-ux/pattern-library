import { Generic } from 'adopted-style-sheets';

/**
 * API
 */
export type RequiredProps = object;

export type OptionalProps = {
	customClass: string;
	ariaHidden: boolean;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = object;

export type OptionalStates = {
	customClass: string;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
