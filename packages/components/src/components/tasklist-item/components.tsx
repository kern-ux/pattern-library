import { Component, h, JSX, Prop, State, Watch, Element, Listen } from '@stencil/core';
import { States, KernTaskListItemStatus } from './schema';
import { IdPropType, LabelPropType, HrefPropType } from '@public-ui/schema';

@Component({
	tag: 'kern-tasklist-item',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernTaskListItem {
	/**
	 * Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.).
	 */
	@Prop() public _label!: LabelPropType;

	/**
	 * Defines if the task is editable or not.
	 */
	@Prop() public _isEditable!: boolean;

	/**
	 * Defines the state of the task.
	 */
	@Prop() public _badge: KernTaskListItemStatus = 'empty';

	/**
	 * Defines the internal ID of the primary component element.
	 */
	@Prop() public _id?: IdPropType;

	/**
	 * Gibt die Ziel-Url des Links an.
	 */
	@Prop() public _href!: HrefPropType;

	@State() public _textId?: string;
	@State() public _badgeId?: string;
	@State() public _hideLabel?: boolean = false;

	@Element() el!: HTMLElement;

	@State() public state: States = {
		_label: '',
		_badge: 'empty',
		_isEditable: false,
		_id: '',
	};

	@Watch('_id')
	public validateId(value?: string): void {
		console.log(value);
		if (typeof value === 'string') {
			this.state._id = value;
		}
	}

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_badge')
	public validateBadge(value?: KernTaskListItemStatus): void {
		if (typeof value === 'string' || value === null) {
			switch (value) {
				case 'complete':
					this.state._badge = 'complete';
					break;
				case 'incomplete':
					this.state._badge = 'incomplete';
					break;
				case 'error':
					this.state._badge = 'error';
					break;
				case 'blocked':
					this.state._badge = 'blocked';
					break;
				case 'empty':
				case null:
					this.state._badge = 'empty';
					break;
			}
		}
	}

	@Watch('_isEditable')
	public validateIsEditable(value?: boolean): void {
		if (typeof value === 'boolean') {
			this.state._isEditable = value;
		}
	}

	// TODO: Überprüfen ob das hier eleganter gelöst werden kann!
	// hier wird Label des buttons ausgeblenden, wenn screen kleiner 600px
	@Listen('resize', { target: 'window' })
	handleResize() {
		if (window.innerWidth < 600) {
			this._hideLabel = true;
		} else {
			this._hideLabel = false;
		}
	}

	public componentWillLoad(): void {
		this.validateId(this._id);

		this._textId = this.state._id ? this.state._id + '_text' : 'default_text_id';
		this._badgeId = this.state._id ? this.state._id + '_badge' : 'default_badge_id';

		this.validateLabel(this._label);
		this.validateBadge(this._badge);
		this.validateIsEditable(this._isEditable);
	}

	private renderStateBadge(): JSX.Element | null {
		switch (this._badge) {
			case 'complete':
				return (
					<kern-badge _id={this._badgeId} _label="Erledigt" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></kern-badge>
				);
			case 'incomplete':
				return <kern-badge _id={this._badgeId} _label="Unvollständig" _status="warning" _icons=""></kern-badge>;
			case 'error':
				return <kern-badge _id={this._badgeId} _label="Fehlerhaft" _status="danger" _icons=""></kern-badge>;
			case 'blocked':
				return <kern-badge _id={this._badgeId} _label="noch nicht zu bearbeiten" _icons="" _status="info"></kern-badge>;
			case 'empty':
			default:
				return null;
		}
	}

	private renderEditButton(): JSX.Element {
		if (this._isEditable) {
			return (
				<kern-link-button
					_aria-description={this._label}
					_label="Bearbeiten"
					_variant="tertiary"
					_icons="{ 'left': { 'icon': 'material-symbols-outlined filled edit' } }"
					_href={this._href}
					_hideLabel={this._hideLabel}
				></kern-link-button>
			);
		} else {
			return false;
		}
	}

	public render(): JSX.Element {
		return (
			<li class="tasklist__item">
				<div class="tasklist__item-header">
					<div class="tasklist__title">
						<kern-text _variant="default" _id={this._textId} _aria-describedBy={this._badgeId}>
							{this._label}
						</kern-text>
					</div>
					<div class="tasklist__status">{this.renderStateBadge()}</div>
					<div class="tasklist__actions">{this.renderEditButton()}</div>
				</div>
			</li>
		);
	}
}
