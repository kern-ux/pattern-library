import { Generic } from 'adopted-style-sheets';
import {
	KoliBriIconsProp,
	Stringified,
	LabelWithExpertSlotPropType,
	InputTypeOnDefault,
	IdPropType,
	NamePropType,
	SuggestionsPropType,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
	W3CInputValue,
	RowsPropType,
	OptionsWithOptgroupPropType,
} from '@public-ui/schema';
import { KernIconProp } from '../icon/schema';

export type RequiredProps = {
	label: string;
};

export type OptionalProps = {
	icons: KernIconProp;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelWithExpertSlotPropType;
};

export type OptionalStates = {
	accessKey: string;
	alert: boolean;
	disabled: boolean;
	error: string;
	hideError: boolean;
	hideLabel: boolean;
	hint: string;
	icons: Stringified<KoliBriIconsProp>;
	id: IdPropType;
	name: NamePropType;
	on: InputTypeOnDefault;
	pattern: string;
	placeholder: string;
	readOnly: boolean;
	required: boolean;
	suggestions: SuggestionsPropType;
	syncValueBySelector: SyncValueBySelectorPropType;
	tabIndex: number;
	tooltipAlign: TooltipAlignPropType;
	touched: boolean;
	rows: RowsPropType;
	options: OptionsWithOptgroupPropType;
	multiple: boolean;
	value: Stringified<W3CInputValue[]>;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
