# kern-heading

<!-- Auto Generated Below -->

## Properties

| Property              | Attribute             | Description                                                                                                            | Type                                                                    | Default     |
| --------------------- | --------------------- | ---------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- | ----------- |
| `_label` _(required)_ | `_label`              | Gibt den Text der Überschrift an.                                                                                      | `string`                                                                | `undefined` |
| `_level`              | `_level`              | Gibt an, welchen H-Level von 1 bis 6 die Überschrift hat. Oder ob es keine Überschrift ist, sondern nur fett gedruckt. | `1 \| 2 \| 3 \| 4 \| 5 \| 6 \| undefined`                               | `1`         |
| `_secondaryHeadline`  | `_secondary-headline` | Gibt den Text der zusätzlichen Überschrift an.                                                                         | `string \| undefined`                                                   | `''`        |
| `_variant`            | `_variant`            | Definiert, welche Variante zur Darstellung verwendet werden soll.                                                      | `"h1" \| "h2" \| "h3" \| "h4" \| "h5" \| "h6" \| "strong" \| undefined` | `undefined` |

## Dependencies

### Used by

- [kern-dialog](../dialog)

### Graph

```mermaid
graph TD;
  kern-dialog --> kern-heading
  style kern-heading stroke:#333,stroke-width:4px
```

---
