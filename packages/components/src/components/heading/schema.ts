import { Generic } from 'adopted-style-sheets';
import { HeadingLevel, HeadingVariantPropType, LabelWithExpertSlotPropType } from '@public-ui/schema';

/** Variant property **/
export type KernHeadingLevel = 1 | 2 | 3 | 4 | 5 | 6;

/**
 * API
 */
export type RequiredProps = {
	label: string;
};

export type OptionalProps = {
	level: KernHeadingLevel;
};
export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelWithExpertSlotPropType;
	level: HeadingLevel;
};
export type OptionalStates = {
	secondaryHeadline: string;
	_variant: HeadingVariantPropType;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
