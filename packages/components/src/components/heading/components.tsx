import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States, KernHeadingLevel } from './schema';
import { HeadingVariantPropType } from '@public-ui/schema';
@Component({
	tag: 'kern-heading',
	shadow: false,
})
export class KernHeading implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	// Props

	/**
	 * Gibt den Text der Überschrift an.
	 */

	@Prop() public _label!: string;

	/**
	 * Gibt an, welchen H-Level von 1 bis 6 die Überschrift hat. Oder ob es keine Überschrift ist, sondern nur fett gedruckt.
	 */

	@Prop() public _level?: KernHeadingLevel = 1;

	/**
	 * Gibt den Text der zusätzlichen Überschrift an.
	 */

	@Prop() public _secondaryHeadline?: string = '';

	/**
	 * Definiert, welche Variante zur Darstellung verwendet werden soll.
	 */
	@Prop() public _variant?: HeadingVariantPropType;

	// States
	@State() public state: States = {
		_label: '…', // required
		_level: 1,
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		// watchString(this, '_label', value);
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_level')
	public validateLevel(value?: KernHeadingLevel): void {
		if (typeof value === 'number') {
			this.state._level = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.validateLevel(this._level);
	}

	public render(): JSX.Element {
		return (
			<kol-heading _label={this.state._label} _secondaryHeadline={this._secondaryHeadline} _level={this.state._level} _variant={this._variant}>
				<slot name="expert" slot="expert" />
			</kol-heading>
		);
	}
}
