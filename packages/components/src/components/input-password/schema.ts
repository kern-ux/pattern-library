import { Generic } from 'adopted-style-sheets';
import {
	ButtonProps,
	IdPropType,
	InputTypeOnDefault,
	InputTypeOnOff,
	KoliBriIconsProp,
	LabelWithExpertSlotPropType,
	Stringified,
	TooltipAlignPropType,
} from '@public-ui/schema';
import { KernIconProp } from '../button/schema';

/**
 * API
 */
export type RequiredProps = {
	label: string;
};

export type OptionalProps = {
	icons: KernIconProp;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelWithExpertSlotPropType;
};

export type OptionalStates = {
	icons: Stringified<KoliBriIconsProp>;
	disabled: boolean;
	accessKey: string;
	autoComplete: InputTypeOnOff;
	error: string;
	hasCounter: boolean;
	hideError: boolean;
	hideLabel: boolean;
	hint: string;
	currentLength: number;
	id: IdPropType;
	maxLength: number;
	name: string;
	on: InputTypeOnDefault;
	placeholder: string;
	readOnly: boolean;
	required: boolean;
	tooltipAlign: TooltipAlignPropType;
	touched: boolean;
	value: string;
	smartButton: Stringified<ButtonProps>;
	hasPasswordToggle: boolean;
	isPasswordVisible: boolean;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
