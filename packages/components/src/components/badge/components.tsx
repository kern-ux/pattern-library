import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch, Host } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States, KernBadgeStatus, KernIconProp } from './schema';

@Component({
	tag: 'kern-badge',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernBadge implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	// Props

	/**
	 * Gibt die ID des Badges an.
	 */

	@Prop() public _id?: string;

	/**
	 * Gibt den Text des Badges an.
	 */

	@Prop() public _label!: string;

	/**
	 * Iconklasse (z.B.: "material-symbols-outlined filled info")
	 */
	@Prop() public _icons!: KernIconProp;

	/**
	 * Gibt den Typ des Badges an.
	 */
	@Prop() public _status: KernBadgeStatus = 'success';

	// States
	@State() public state: States = {
		_color: {
			backgroundColor: '#000',
			foregroundColor: '#fff',
		},
		_icons: {},
		_label: '…', // required
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		// watchString(this, '_label', value);
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_icons')
	public validateIcons(value?: string): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	@Watch('_status')
	public validateStatus(value?: KernBadgeStatus): void {
		if (typeof value === 'string') {
			switch (value) {
				case 'success':
					this.state._color = {
						backgroundColor: '#D9F7F1',
						foregroundColor: '#206D5C',
					};
					break;
				case 'warning':
					this.state._color = {
						backgroundColor: '#FDFACC',
						foregroundColor: '#7B7301',
					};
					break;
				case 'info':
					this.state._color = {
						backgroundColor: '#E8EEFC',
						foregroundColor: '#546690',
					};
					break;
				case 'danger':
				default:
					this.state._color = {
						backgroundColor: '#FFDADA',
						foregroundColor: '#B33030',
					};
					break;
			}
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.validateIcons(this._icons);
		this.validateStatus(this._status);
	}

	public render(): JSX.Element {
		return (
			<Host class={{ [this._status]: true }}>
				<kol-badge _id={this._id} class={{ [this._status]: true }} _icons={this.state._icons} _color={this.state._color} _label={this.state._label}></kol-badge>
			</Host>
		);
	}
}
