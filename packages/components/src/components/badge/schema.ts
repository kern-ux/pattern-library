import { Generic } from 'adopted-style-sheets';
import { KoliBriIconsProp, LabelPropType, PropColor, Stringified } from '@public-ui/schema';

/** Icon type for any icon Class **/
export type KernIconProp = string;

export type KernBadgeStatus = 'success' | 'warning' | 'danger' | 'info';

/**
 * API
 */
export type RequiredProps = {
	label: string;
	status: KernBadgeStatus;
};

export type OptionalProps = {
	icons: KernIconProp;
};
export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelPropType;
};

export type OptionalStates = {
	color: PropColor;
	icons: Stringified<KoliBriIconsProp>;
};

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
