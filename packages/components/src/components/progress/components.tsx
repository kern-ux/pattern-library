import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { Generic } from 'adopted-style-sheets';
import { States, OptionalProps, OptionalStates, RequiredProps, RequiredStates, KernProgressVariant } from './schema';
import { LabelPropType } from '@public-ui/schema';

@Component({
	tag: 'kern-progress',
	styleUrls: {
		default: './style.css',
	},
	shadow: false,
})
export class KernProgress implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Definiert den Text, der die Fortschrittsanzeige beschreibt.
	 */
	@Prop() public _label?: LabelPropType;

	/**
	 * Definiert bei welchem Wert die Fortschrittsanzeige abgeschlossen ist.
	 */
	@Prop() public _max!: number;

	/**
	 * Definiert die Einheit der Schritt-Werte (wird nicht angezeigt).
	 */
	@Prop() public _unit?: string = '%';

	/**
	 * Definiert den Fortschritt.
	 */
	@Prop() public _value!: number;

	/**
	 * Definiert, welche Variante zur Darstellung verwendet werden soll.
	 */
	@Prop() public _variant?: KernProgressVariant = 'bar';

	@State() public state: States = {
		_max: 100,
		_unit: '%',
		_value: 0,
		_variant: 'bar',
		_liveValue: 0,
	};

	@Watch('_label')
	public validateLabel(value?: LabelPropType): void {
		// watchString(this, '_label', value);
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_max')
	public validateMax(value?: number): void {
		if (typeof value === 'number') {
			this.state._max = value;
		}
	}

	@Watch('_unit')
	public validateUnit(value?: string): void {
		if (typeof value === 'string') {
			this.state._unit = value;
		}
	}

	@Watch('_value')
	public validateValue(value?: number): void {
		if (typeof value === 'number') {
			this.state._value = value;
		}
	}

	@Watch('_variant')
	public validateVariant(value?: KernProgressVariant): void {
		if (typeof value === 'string') {
			switch (value) {
				case 'bar':
					this.state._variant = 'bar';
					break;
				case 'cycle':
					this.state._variant = 'cycle';
					break;
				case 'cycle-value-label':
					this.state._variant = 'cycle';
					break;
				case 'cycle-label-value':
					this.state._variant = 'cycle';
					break;
			}
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.validateMax(this._max);
		this.validateUnit(this._unit);
		this.validateValue(this._value);
		this.validateVariant(this._variant);
	}

	public render(): JSX.Element {
		return (
			<kol-progress
				_value={this.state._value}
				_unit={this.state._unit}
				_label={this.state._label}
				_variant={this.state._variant}
				_max={this.state._max}
			></kol-progress>
		);
	}
}
