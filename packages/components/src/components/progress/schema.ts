import { Generic } from 'adopted-style-sheets';
import { KoliBriProgressVariantType } from '@public-ui/schema';

export type KernProgressVariant = 'bar' | 'cycle' | 'cycle-value-label' | 'cycle-label-value';

export type RequiredProps = {
	max: number;
	value: number;
};

export type OptionalProps = {
	label: string;
	unit: string;
	variant: KernProgressVariant;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps & {
	liveValue: number;
};
export type OptionalStates = {
	label: string;
	unit: string;
	variant: KoliBriProgressVariantType;
};

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
