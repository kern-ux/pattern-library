# kern-progress

<!-- Auto Generated Below -->

## Properties

| Property              | Attribute  | Description                                                           | Type                                                                          | Default     |
| --------------------- | ---------- | --------------------------------------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `_label`              | `_label`   | Definiert den Text, der die Fortschrittsanzeige beschreibt.           | `string \| undefined`                                                         | `undefined` |
| `_max` _(required)_   | `_max`     | Definiert bei welchem Wert die Fortschrittsanzeige abgeschlossen ist. | `number`                                                                      | `undefined` |
| `_unit`               | `_unit`    | Definiert die Einheit der Schritt-Werte (wird nicht angezeigt).       | `string \| undefined`                                                         | `'%'`       |
| `_value` _(required)_ | `_value`   | Definiert den Fortschritt.                                            | `number`                                                                      | `undefined` |
| `_variant`            | `_variant` | Definiert, welche Variante zur Darstellung verwendet werden soll.     | `"bar" \| "cycle" \| "cycle-label-value" \| "cycle-value-label" \| undefined` | `'bar'`     |

---
