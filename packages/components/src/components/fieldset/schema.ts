import { Generic } from 'adopted-style-sheets';

export type KernOrientation = 'horizontal' | 'vertical';
/**
 * API
 */
export type RequiredProps = {};
export type OptionalProps = {
	customClass: string;
	legend: string;
	orientation: KernOrientation;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps;
export type OptionalStates = {
	customClass: string;
	legend: string;
	orientation: KernOrientation;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
