import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { KernLevelVariant, KernAlertVariant, OptionalProps, OptionalStates, RequiredProps, RequiredStates, States, KernAlertType } from './schema';
import { KoliBriAlertEventCallbacks, LabelPropType } from '@public-ui/schema';

@Component({
	tag: 'kern-alert',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernAlert implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Defines whether the screen-readers should read out the notification.
	 */
	@Prop() public _alert?: boolean = false;

	/**
	 * Gibt den Text des Alerts an.
	 */
	@Prop() public _label!: LabelPropType;

	/**
	 * Defines whether the element can be closed.
	 * @TODO: Change type back to `HasCloserPropType` after Stencil#4663 has been resolved.
	 */
	@Prop() public _hasCloser?: boolean = false;

	/**
	 * Gibt den Typ des Alerts an.
	 */
	@Prop() public _type?: KernAlertType = 'success';

	/**
	 * Gibt die Variante des Alerts an.
	 */
	@Prop() public _variant?: KernAlertVariant = 'msg';

	/**
	 * Gibt die Wertigkeit der Alert Überschrift an.
	 */
	@Prop() public _level?: KernLevelVariant = 5;

	/**
	 * Gibt die EventCallback-Function für das Schließen des Alerts an.
	 */
	@Prop() public _on?: KoliBriAlertEventCallbacks;

	/**
	 * Gibt die Möglichkeit, eine eigene CSS Klasse zu ergänzen.
	 */
	@Prop() public _customClass?: string;

	@State() public state: States = {
		_label: '…', // required
	};

	@Watch('_label')
	public validateLabel(value?: LabelPropType): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_level')
	public validateLevel(value?: KernLevelVariant): void {
		if (typeof value === 'number') {
			this.state._level = value;
		}
	}

	@Watch('_variant')
	public validateVariant(value?: KernAlertVariant): void {
		if (typeof value === 'string') {
			this.state._variant = value;
		}
	}

	@Watch('_type')
	public validateType(value?: KernAlertType): void {
		if (typeof value === 'string') {
			switch (value) {
				case 'warning':
					this.state._type = 'warning';
					break;
				case 'success':
					this.state._type = 'success';
					break;
				case 'info':
					this.state._type = 'info';
					break;
				case 'error':
					this.state._type = 'error';
					break;
				case 'form-error':
					this.state._type = 'default';
					break;
			}
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.validateType(this._type);
		this.validateLevel(this._level);
		this.validateVariant(this._variant);
	}

	public render(): JSX.Element {
		return (
			<kol-alert
				_alert={this._alert}
				_on={this._on}
				_custom-class={this.state._customClass}
				_type={this.state._type}
				_variant={this.state._variant}
				_label={this.state._label}
				_level={5}
				_hasCloser={this._hasCloser}
			>
				<slot />
			</kol-alert>
		);
	}
}
