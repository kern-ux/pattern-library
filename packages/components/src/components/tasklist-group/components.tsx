import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { LabelPropType, IdPropType } from '@public-ui/schema';

@Component({
	tag: 'kern-tasklist-group',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernTaskListGroup implements Generic.Element.ComponentApi<RequiredProps, NonNullable<unknown>, RequiredStates, OptionalStates> {
	/**
	 * Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.).
	 */
	@Prop() public _label!: LabelPropType;

	/**
	 * Gibt die ID an.
	 */
	@Prop() public _id!: IdPropType;

	@State() public state: States = {
		_label: '',
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}
	public componentWillLoad(): void {
		this.validateLabel(this._label);
	}

	public render(): JSX.Element {
		return (
			<div class="tasklist__group">
				<div class="tasklist__group-header">
					<kol-heading _level="4" _label={this.state._label}></kol-heading>
				</div>

				<slot></slot>
			</div>
		);
	}
}
