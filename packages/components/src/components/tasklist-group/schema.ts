import { Generic } from 'adopted-style-sheets';
import { IdPropType, KoliBriModalEventCallbacks, LabelPropType } from '@public-ui/schema';

/**
 * API
 */
export type RequiredProps = {
	label: LabelPropType;
};

export type OptionalProps = {
	activeElement: HTMLElement | null;
	on: KoliBriModalEventCallbacks;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps;

export type OptionalStates = {
	on: KoliBriModalEventCallbacks;
	id: IdPropType;
};

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
