import { mixMembers } from 'stencil-awesome-test';
import { Props, States } from '../schema';
import clsx from 'clsx';
export const getButtonHtml = (props: Props): string => {
	const state = mixMembers<Props, States>(
		{
			_variant: 'primary',
			_type: 'button',
			_tooltipAlign: 'top',
		},
		props,
	);
	const variant = typeof state._variant === 'string' ? state._variant : 'primary';
	// eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-assignment
	const classNames = clsx({
		button: true,
		disabled: state._disabled,
		'hide-label': state._hideLabel,
		[variant]: true,
	});
	return `
<kern-button>
	<kol-button
	class="${classNames}"
		${props._label ? ` _label="${props._label}"` : ''}
		${state._variant ? ` _variant="${state._variant}"` : ''}
		${state._tooltipAlign ? ` _tooltipAlign="${state._tooltipAlign}"` : ''}
		${state._type ? ` _type="${state._type}"` : ''}
	></kol-button>
</kern-button>`;
};
