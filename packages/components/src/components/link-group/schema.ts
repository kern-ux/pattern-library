import { Generic } from 'adopted-style-sheets';
import { LabelPropType, LinkProps, ListStyleType, Orientation, Stringified } from '@public-ui/schema';
import { KernLinkProps } from '../link/schema';

export type KernOrientation = 'horizontal' | 'vertical';

export type KernListStyleType = 'disc' | 'circle' | 'none';
/**
 * API
 */
export type RequiredProps = {
	links: Stringified<KernLinkProps[]>;
	label: string;
};
export type OptionalProps = {
	orientation: KernOrientation;
	listStyleType: KernListStyleType;
};
export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	links: LinkProps[];
	label: LabelPropType;
};
export type OptionalStates = {
	orientation: Orientation;
	listStyleType: ListStyleType;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
