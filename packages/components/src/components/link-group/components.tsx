import { Component, h, Host, JSX, Prop, State, Watch } from '@stencil/core';
import { Generic } from 'adopted-style-sheets';
import { KernListStyleType, KernOrientation, OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { Stringified, LinkProps } from '@public-ui/schema';
import { watchString } from '../../utils/prop.validators';

// import {KernLinkProps} from "../link/schema";

@Component({
	tag: 'kern-link-group',
	styleUrls: {
		default: './style.css',
	},
	shadow: false,
})
export class KernLinkGroup implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.).
	 */
	@Prop() public _label!: string;

	/**
	 * Defines whether the orientation of the component is horizontal or vertical.
	 */
	@Prop() public _orientation?: KernOrientation = 'vertical';

	/**
	 * Gibt den List-Style-Typen für ungeordnete Listen aus. Wird bei horizontalen LinkGroups als Trenner verwendet
	 */
	@Prop() public _listStyleType?: KernListStyleType = 'disc';

	/**
	 * Defines the list of links to render.
	 */
	@Prop() public _links!: LinkProps[];

	@State() public state: States = {
		_label: '...',
		_links: [],
		_listStyleType: 'disc',
		_orientation: 'vertical',
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		watchString(this, '_label', value, {
			required: true,
		});
	}

	@Watch('_orientation')
	public validateOrientation(value?: string): void {
		if (typeof value === 'string') {
			switch (value) {
				case 'horizontal':
					this.state._orientation = 'horizontal';
					break;
				case 'vertical':
				default:
					this.state._orientation = 'vertical';
			}
		}
	}

	@Watch('_listStyleType')
	public validateListStyleType(value?: string): void {
		if (typeof value === 'string') {
			switch (value) {
				case 'square':
				case 'disc':
					this.state._listStyleType = 'disc';
					break;
				case 'circle':
					this.state._listStyleType = 'circle';
					break;
				case 'decimal':
				case 'decimal-leading-zero':
				case 'lower-alpha':
				case 'lower-latin':
				case 'lower-greek':
				case 'lower-roman':
				case 'upper-alpha':
				case 'upper-latin':
				case 'upper-roman':
				default:
					this.state._listStyleType = 'none';
			}
		}
	}

	@Watch('_links')
	public validateLinks(value?: Stringified<LinkProps[]>): void {
		if (typeof value === 'object') {
			this.state._links = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		// this.validateLinks(this._links);
		this.validateOrientation(this._orientation);
		this.validateListStyleType(this._listStyleType);
	}

	public render(): JSX.Element {
		console.log(this.state._links);

		return (
			<Host>
				<kol-link-group
					_label={this.state._label}
					_orientation={this.state._orientation}
					_listStyleType={this.state._listStyleType}
					aria-label={this.state._label}
					_links={this.state._links}
				>
					<slot />
				</kol-link-group>
			</Host>
		);
	}
}
