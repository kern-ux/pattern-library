import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, State, Prop, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { ShowPropType, SpinVariantPropType, validateShow, validateSpinVariant } from '@public-ui/schema';

@Component({
	tag: 'kern-loader',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernLoader implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	// Props
	/**
	 * Makes the element show up.
	 * @TODO: Change type back to `ShowPropType` after Stencil#4663 has been resolved.
	 */
	@Prop({ mutable: true, reflect: true }) public _show?: boolean = false;

	/**
	 * Defines which variant should be used for presentation.
	 */
	@Prop() public _variant?: SpinVariantPropType = 'cycle';

	// States
	@State() public state: States = {
		_variant: 'cycle',
	};

	@Watch('_show')
	public validateShow(value?: ShowPropType): void {
		validateShow(this, value);
	}

	@Watch('_variant')
	public validateVariant(value?: SpinVariantPropType): void {
		validateSpinVariant(this, value);
	}

	public componentWillLoad(): void {}

	public render(): JSX.Element {
		return <kol-spin _show={this._show} _variant={this._variant}></kol-spin>;
	}
}
