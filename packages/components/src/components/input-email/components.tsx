import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, State, Prop, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { KernIconProp } from '../button/schema';
import {
	ButtonProps,
	IdPropType,
	InputTypeOnDefault,
	InputTypeOnOff,
	LabelWithExpertSlotPropType,
	NamePropType,
	Stringified,
	SuggestionsPropType,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';

@Component({
	tag: 'kern-input-email',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernInputEmail implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Definiert, welche Tastenkombination verwendet werden kann, um das interaktive Element des Komponenten auszulösen oder zu fokussieren.
	 */
	@Prop() public _accessKey?: string;

	/**
	 * Definiert, ob die Bildschirmleser die Benachrichtigung vorlesen sollen.
	 */
	@Prop({ mutable: true, reflect: true }) public _alert?: boolean = true;

	/**
	 * Legt fest, ob das Eingabefeld automatisch vervollständigt werden kann.
	 */
	@Prop() public _autoComplete?: InputTypeOnOff;

	/**
	 * Macht das Element nicht fokussierbar und ignoriert alle Ereignisse.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Definiert den Text der Fehlermeldung.
	 */
	@Prop() public _error?: string;

	/**
	 * Zeigt die Zeichenanzahl an der unteren Grenze der Eingabe an.
	 */
	@Prop() public _hasCounter?: boolean = false;

	/**
	 * Versteckt die Fehlermeldung, lässt sie aber im DOM für das aria-describedby der Eingabe.
	 */
	@Prop({ mutable: true, reflect: true }) public _hideError?: boolean = false;

	/**
	 * Versteckt die Beschriftung standardmäßig und zeigt den Beschriftungstext mit einem Tooltip an, wenn das interaktive Element fokussiert ist oder die Maus darüber ist.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Definiert den Hinweistext.
	 */
	@Prop() public _hint?: string = '';

	/**
	 * Definiert die interne ID des primären Komponentenelements.
	 */
	@Prop() public _id?: IdPropType;

	/**
	 * Definiert das sichtbare oder semantische Label der Komponente (z.B. aria-label, label, Überschrift, Beschriftung, Zusammenfassung, etc.). Setzen Sie auf `false`, um den Experten-Slot zu aktivieren.
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Gibt die maximale Anzahl von Eingabezeichen an.
	 */
	@Prop() public _maxLength?: number;

	/**
	 * Definiert den technischen Namen eines Eingabefelds.
	 */
	@Prop() public _name?: NamePropType;

	/**
	 * Gibt die EventCallback-Funktionen für das Input-Event an.
	 */
	@Prop() public _on?: InputTypeOnDefault;

	/**
	 * Definiert ein Validierungsmuster für das Eingabefeld.
	 */
	@Prop() public _pattern?: string;

	/**
	 * Gibt den Placeholder des Inputs an.
	 */
	@Prop() public _placeholder!: string;

	/**
	 * Macht das Eingabeelement schreibgeschützt.
	 */
	@Prop() public _readOnly?: boolean = false;

	/**
	 * Macht das Eingabeelement erforderlich.
	 */
	@Prop() public _required?: boolean = false;

	/**
	 * Setzt die Iconklasse (z.B.: `_icon="material-symbols-outlined home"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Vorschläge, die für die Eingabe bereitgestellt werden.
	 */
	@Prop() public _suggestions?: SuggestionsPropType;

	/**
	 * Ermöglicht das Hinzufügen eines Buttons mit einer beliebigen Aktion innerhalb des Elements (_hide-label nur).
	 */
	@Prop() public _smartButton?: Stringified<ButtonProps>;

	/**
	 * Selektor zur Synchronisierung des Wertes mit einem anderen Eingabeelement.
	 */
	@Prop() public _syncValueBySelector?: SyncValueBySelectorPropType;

	/**
	 * Definiert, welchen Tab-Index das primäre Element der Komponente hat. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Definiert, wo der Tooltip vorzugsweise angezeigt werden soll: oben, rechts, unten oder links.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'top';

	/**
	 * Zeigt an, ob die Eingabe von einem Benutzer berührt wurde.
	 */
	@Prop({ mutable: true, reflect: true }) public _touched?: boolean = false;

	/**
	 * Definiert den Wert des Inputs.
	 */
	@Prop({ mutable: true }) public _value?: string;

	// States
	@State() public state: States = {
		_label: '', // ⚠ required
		_placeholder: '',
		_icons: {},
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_placeholder')
	public validatePlaceholder(value?: string): void {
		if (typeof value === 'string') {
			this.state._placeholder = value;
		}
	}

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.validateIcons(this._icons);
		this.validatePlaceholder(this._placeholder);
	}

	public render(): JSX.Element {
		return (
			<kol-input-email
				class={{
					'hide-label': this._hideLabel === true,
				}}
				_placeholder={this.state._placeholder}
				_accessKey={this._accessKey}
				_disabled={this._disabled}
				_error={this._error}
				_hideError={this._hideError}
				_hasCounter={this._hasCounter}
				_hideLabel={this._hideLabel}
				_hint={this._hint}
				_icons={this.state._icons}
				_id={this._id}
				_on={this._on}
				_label={this.state._label}
				_suggestions={this._suggestions}
				_maxLength={this._maxLength}
				_readOnly={this._readOnly}
				_required={this._required}
				_smartButton={this._smartButton}
				_tooltipAlign={this._tooltipAlign}
				_touched={this._touched}
				_name={this._name}
				_value={this._value}
			></kol-input-email>
		);
	}
}
