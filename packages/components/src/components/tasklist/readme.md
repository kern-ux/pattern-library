# kern-tasklist

<!-- Auto Generated Below -->

## Properties

| Property              | Attribute | Description                                                                                                        | Type                  | Default     |
| --------------------- | --------- | ------------------------------------------------------------------------------------------------------------------ | --------------------- | ----------- |
| `_id`                 | `_id`     | Defines the internal ID of the primary component element.                                                          | `string \| undefined` | `undefined` |
| `_label` _(required)_ | `_label`  | Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.). | `string`              | `undefined` |

---
