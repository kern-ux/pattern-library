import { Generic } from 'adopted-style-sheets';
import { IdPropType, KoliBriModalEventCallbacks, LabelPropType } from '@public-ui/schema';

/**
 * API
 */
export type RequiredProps = {
	label: LabelPropType;
};

export type OptionalProps = {
	activeElement: HTMLElement | null;
	on: KoliBriModalEventCallbacks;
	width: string;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps;

export type OptionalStates = {
	activeElement: HTMLElement | null;
	on: KoliBriModalEventCallbacks;
	width: string;
	id: IdPropType;
};

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
