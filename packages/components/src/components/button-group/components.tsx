import { Component, h, Host, JSX, State } from '@stencil/core';
import { Generic } from 'adopted-style-sheets';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';

@Component({
	tag: 'kern-button-group',
	styleUrls: {
		default: './style.css',
	},
	shadow: false,
})
export class KernButtonGroup implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	@State() public state: States = {};

	public render(): JSX.Element {
		return (
			<Host>
				<kol-button-group>
					<slot />
				</kol-button-group>
			</Host>
		);
	}
}
