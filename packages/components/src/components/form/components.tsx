import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State } from '@stencil/core';
import { OptionalStates, RequiredStates, States } from './schema';
import { ErrorListPropType, KoliBriFormCallbacks, Stringified } from '@public-ui/schema';
@Component({
	tag: 'kern-form',
	shadow: false,
})
export class KernForm implements Generic.Element.ComponentApi<NonNullable<unknown>, NonNullable<unknown>, RequiredStates, OptionalStates> {
	/**
	 * Gibt die EventCallback-Funktionen für die Form-Events an.
	 */
	@Prop() public _on?: KoliBriFormCallbacks;

	/**
	 * Defines whether the mandatory-fields-hint should be shown. A string overrides the default text.
	 */
	@Prop() public _requiredText?: Stringified<boolean> = true;
	/**
	 * A list of error objects that each describe an issue encountered in the form.
	 * Each error object contains a message and a selector for identifying the form element related to the error.
	 */
	@Prop() public _errorList?: ErrorListPropType[];

	// States
	@State() public state: States = {};

	public render(): JSX.Element {
		return (
			<kol-form _on={this._on} _requiredText={this._requiredText} _errorList={this._errorList}>
				<slot />
			</kol-form>
		);
	}
}
