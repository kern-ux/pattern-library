import { Generic } from 'adopted-style-sheets';
import { ErrorListPropType, KoliBriFormCallbacks, Stringified } from '@public-ui/schema';

export type RequiredProps = NonNullable<unknown>;

export type OptionalProps = {
	errorList: ErrorListPropType[];
	requiredText: Stringified<boolean>;
	on: KoliBriFormCallbacks;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps;

export type OptionalStates = {
	on: KoliBriFormCallbacks;
	errorList: ErrorListPropType[];
	requiredText: Stringified<boolean>;
};

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
