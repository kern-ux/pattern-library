import { IdPropType } from '@public-ui/schema';
import { Generic } from 'adopted-style-sheets';

export type KernTextVariant = 'default' | 'large' | 'small';
export type KernTextWeight = 'normal' | 'bold';

/**
 * API
 */
export type RequiredProps = {
	variant: KernTextVariant;
	weight: KernTextWeight;
};
export type OptionalProps = {
	customClass: string;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	variant: KernTextVariant;
	weight: KernTextWeight;
};
export type OptionalStates = {
	customClass: string;
	id: IdPropType;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
