import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch, Host } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States, KernTextVariant, KernTextWeight } from './schema';
import { watchString } from '../../utils/prop.validators';

import { IdPropType } from '@public-ui/schema';

@Component({
	tag: 'kern-text',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernText implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Gibt an, welche Ausprägung der Text hat.
	 */
	@Prop() public _variant: KernTextVariant = 'default';

	/**
	 * Gibt an, welche Stärke der Text hat.
	 */
	@Prop() public _weight: KernTextWeight = 'normal';

	/**
	 * Gibt an, welche Custom-Class übergeben werden soll, wenn _variant="custom" gesetzt ist.
	 */
	@Prop() public _customClass?: string;

	/**
	 * aria-describedBy Id
	 */
	@Prop() public _ariaDescribedBy?: string;

	/**
	 * Definiert die ID des interaktiven Elements der Komponente.
	 */
	@Prop() public _id?: IdPropType;

	@State() public state: States = {
		_variant: 'default',
		_weight: 'normal',
	};

	@Watch('_customClass')
	public validateCustomClass(value?: string): void {
		watchString(this, '_customClass', value, {
			defaultValue: undefined,
		});
	}

	@Watch('_variant')
	public validateVariant(value?: KernTextVariant): void {
		if (typeof value === 'string') {
			this.state._variant = value;
		}
	}

	@Watch('_weight')
	public validateWeight(value?: KernTextWeight): void {
		if (typeof value === 'string') {
			this.state._weight = value;
		}
	}

	public componentWillLoad(): void {
		this.validateCustomClass(this._customClass);
	}

	public render(): JSX.Element {
		const classVariant = this._variant === 'default' ? '' : this._variant;
		const classWeight = this._weight === 'normal' ? '' : this._weight;
		const className = `${classVariant} ${classWeight}`;
		return (
			<Host class={this._variant}>
				<p id={this._id} aria-describedby={this._ariaDescribedBy} class={className}>
					<slot />
				</p>
			</Host>
		);
	}
}
