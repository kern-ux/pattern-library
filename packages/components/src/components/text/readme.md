# kern-text

<!-- Auto Generated Below -->

## Properties

| Property           | Attribute            | Description                                                                              | Type                              | Default     |
| ------------------ | -------------------- | ---------------------------------------------------------------------------------------- | --------------------------------- | ----------- |
| `_ariaDescribedBy` | `_aria-described-by` | aria-describedBy Id                                                                      | `string \| undefined`             | `undefined` |
| `_customClass`     | `_custom-class`      | Gibt an, welche Custom-Class übergeben werden soll, wenn \_variant="custom" gesetzt ist. | `string \| undefined`             | `undefined` |
| `_id`              | `_id`                | Definiert die ID des interaktiven Elements der Komponente.                               | `string \| undefined`             | `undefined` |
| `_variant`         | `_variant`           | Gibt an, welche Ausprägung der Text hat.                                                 | `"default" \| "large" \| "small"` | `'default'` |
| `_weight`          | `_weight`            | Gibt an, welche Stärke der Text hat.                                                     | `"bold" \| "normal"`              | `'normal'`  |

## Dependencies

### Used by

- [kern-tasklist-item](../tasklist-item)

### Graph

```mermaid
graph TD;
  kern-tasklist-item --> kern-text
  style kern-text stroke:#333,stroke-width:4px
```

---
