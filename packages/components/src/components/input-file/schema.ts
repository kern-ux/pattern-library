import { Generic } from 'adopted-style-sheets';
import {
	Stringified,
	LabelWithExpertSlotPropType,
	InputTypeOnDefault,
	IdPropType,
	NamePropType,
	ButtonProps,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
	KoliBriHorizontalIcons,
} from '@public-ui/schema';
import { KernIconProp } from '../icon/schema';

export type maxFileSize = number | undefined;

export type AllowedFileTypes = ['jpg' | 'png' | 'jpeg'];

export type RequiredProps = {
	label: string;
};

export type OptionalProps = {
	icons: KernIconProp;
	maxFileSize: maxFileSize;
	allowedFileTypes: AllowedFileTypes[];
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelWithExpertSlotPropType;
};

export type OptionalStates = {
	accept: string;
	accessKey: string;
	alert: boolean;
	disabled: boolean;
	error: string;
	hideError: boolean;
	hideLabel: boolean;
	hint: string;
	icons: Stringified<KoliBriHorizontalIcons>;
	id: IdPropType;
	multiple: boolean;
	name: NamePropType;
	on: InputTypeOnDefault;
	required: boolean;
	smartButton: Stringified<ButtonProps>;
	syncValueBySelector: SyncValueBySelectorPropType;
	tabIndex: number;
	tooltipAlign: TooltipAlignPropType;
	touched: boolean;
	value: string;
	maxFileSize: maxFileSize;
	allowedFileTypes: AllowedFileTypes[];
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
