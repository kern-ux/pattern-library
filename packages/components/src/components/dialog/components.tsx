import { Generic } from 'adopted-style-sheets';
import { Component, h, Host, JSX, Prop, State, Watch, Element } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { HasCloserPropType, IdPropType, LabelPropType } from '@public-ui/schema';
import { watchString } from '../../utils/prop.validators';

@Component({
	tag: 'kern-dialog',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernDialog implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	@Element() hostElement!: HTMLElement;
	/**
	 * Defines the internal ID of the primary component element.
	 */
	@Prop() public _id?: IdPropType;

	/**
	 * Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.).
	 */
	@Prop() public _label!: LabelPropType;

	/**
	 * Gibt an, welche Custom-Class übergeben werden soll, wenn _variant="custom" gesetzt ist.
	 */
	@Prop() public _customClass?: string;

	/**
	 * Soll schließen Button angezeigt werden?
	 */
	@Prop() public _hasCloser?: HasCloserPropType;

	@State() public state: States = {
		_label: '',
		_hasCloser: false,
	};

	@Watch('_id')
	public validateId(value?: IdPropType): void {
		watchString(this, '_id', value, {
			defaultValue: undefined,
		});
	}

	@Watch('_label')
	public validateLabel(value?: string): void {
		watchString(this, '_label', value, {
			defaultValue: undefined,
		});
	}

	@Watch('_customClass')
	public validateCustomClass(value?: string): void {
		watchString(this, '_customClass', value, {
			defaultValue: undefined,
		});
	}

	@Watch('_hasCloser')
	public validateHasCloser(value?: HasCloserPropType): void {
		if (typeof value === 'boolean') {
			this.state._hasCloser = value;
		}
	}

	public componentWillLoad(): void {
		this.validateId(this._id);
		this.validateCustomClass(this._customClass);
		this.validateLabel(this._label);
		this.validateHasCloser(this._hasCloser);
	}

	private handleCloseClick = (): void => {
		const dialogElement = this.hostElement.querySelector('dialog');
		if (dialogElement) {
			dialogElement.close();
		}
	};

	public render(): JSX.Element {
		return (
			<Host _id={this.state._id} _customClass={this.state._customClass}>
				<dialog id={this.state._id}>
					{this.state._hasCloser && (
						<kern-button
							_hide-label="true"
							_label="Dialog Schließen"
							_variant="tertiary"
							_icons="material-symbols-rounded filled close"
							_on={{ onClick: this.handleCloseClick }}
						></kern-button>
					)}
					<div class="content">
						<kern-heading _level={2} _label={this.state._label} />
						<slot />
					</div>
				</dialog>
			</Host>
		);
	}
}
