import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States, KernIconProp } from './schema';
import {
	AccessKeyPropType,
	AlternativeButtonLinkRolePropType,
	AriaCurrentValuePropType,
	CustomClassPropType,
	DownloadPropType,
	HrefPropType,
	LabelWithExpertSlotPropType,
	LinkOnCallbacksPropType,
	LinkTargetPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';
import { KernButtonVariant } from '../button/schema';

@Component({
	tag: 'kern-link-button',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernLinkButton implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Defines the elements access key.
	 */
	@Prop() public _accessKey?: AccessKeyPropType;

	/**
	 * Defines the value for the aria-current attribute.
	 */
	@Prop() public _ariaCurrentValue?: AriaCurrentValuePropType;

	/**
	 * Makes the element not focusable and ignore all events.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Teilt dem Browser mit, dass sich hinter dem Link eine Datei befindet. Setzt optional den Dateinamen.
	 */
	@Prop() public _download?: DownloadPropType;

	/**
	 * Blendet die Beschriftung (Label) aus und zeigt sie stattdessen mittels eines Tooltips an.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Gibt die Ziel-Url des Links an.
	 */
	@Prop() public _href!: HrefPropType;

	/**
	 * Setzt die sichtbare oder semantische Beschriftung der Komponente (z.B. Aria-Label, Label, Headline, Caption, Summary usw.).
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Setzt die Iconklasse (z.B.: `_icon="material-symbols-outlined home"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Defines the callback functions for links.
	 */
	@Prop() public _on?: LinkOnCallbacksPropType;

	/**
	 * Defines the role of the components primary element.
	 */
	@Prop() public _role?: AlternativeButtonLinkRolePropType;

	/**
	 * Defines which tab-index the primary element of the component has. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Defines where to open the link.
	 */
	@Prop() public _target?: LinkTargetPropType;

	/**
	 * Gibt an, welche Ausprägung der Button hat.
	 */
	@Prop() public _variant?: KernButtonVariant = 'primary';

	/**
	 * Gibt an, welche Custom-Class übergeben werden soll, wenn _variant="custom" gesetzt ist.
	 */
	@Prop() public _customClass?: CustomClassPropType;

	/**
	 * Defines where to show the Tooltip preferably: top, right, bottom or left.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'right';

	@State() public state: States = {
		_href: '…', // ⚠ required
		_icons: {},
		_label: '',
	};

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	@Watch('_href')
	public validateHref(value?: string): void {
		if (typeof value === 'string') {
			this.state._href = value;
		}
	}

	@Watch('_variant')
	public validateVariant(value?: KernButtonVariant): void {
		if (typeof value === 'string') {
			switch (value) {
				case 'primary':
					this.state._variant = 'primary';
					break;
				case 'secondary':
					this.state._variant = 'secondary';
					break;
				case 'tertiary':
					this.state._variant = 'ghost';
					break;
				case 'custom':
					this.state._variant = 'custom';
					break;
			}
		}
	}

	public componentWillLoad(): void {
		this.validateIcons(this._icons);
		this.validateHref(this._href);
		this.validateVariant(this._variant);
	}

	public render(): JSX.Element {
		return (
			<kol-link-button
				class={{
					button: true,
					[this._variant as string]: this._variant !== 'custom',
					[this._customClass as string]: this._variant === 'custom' && typeof this._customClass === 'string' && this._customClass.length > 0,
				}}
				_href={this.state._href}
				_hide-label={this.state._hideLabel}
				_label={this._label || this.state._href}
				_icons={this.state._icons}
				_accessKey={this._accessKey}
				_ariaCurrentValue={this._ariaCurrentValue}
				_disabled={this._disabled}
				_download={this._download}
				_hideLabel={this._hideLabel}
				_on={this._on}
				_role={this._role}
				_tabIndex={this._tabIndex}
				_target={this._target}
				_variant={this.state._variant}
				_tooltipAlign={this._tooltipAlign}
			>
				<slot name="expert" slot="expert" />
			</kol-link-button>
		);
	}
}
