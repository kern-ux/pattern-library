# kern-kopfzeile

<!-- Auto Generated Below -->

## Properties

| Property    | Attribute   | Description                                    | Type                    | Default     |
| ----------- | ----------- | ---------------------------------------------- | ----------------------- | ----------- |
| `_fluid`    | `_fluid`    | Setzt die Breite der Kopfzeile auf 100%.       | `boolean \| undefined`  | `false`     |
| `_label`    | `_label`    | Gibt den Text der Überschrift an.              | `string \| undefined`   | `undefined` |
| `_language` | `_language` | Gibt an, welche Sprache verwendet werden soll. | `"english" \| "german"` | `'german'`  |

---
