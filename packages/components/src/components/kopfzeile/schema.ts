import { Generic } from 'adopted-style-sheets';

export type KernKopfzeileLanguage = 'german' | 'english';

export type RequiredProps = {
	language: KernKopfzeileLanguage;
};

export type OptionalProps = {
	label: string;
	fluid: boolean;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps;

export type OptionalStates = {
	label: string;
	fluid: boolean;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
