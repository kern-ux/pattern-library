import { Generic } from 'adopted-style-sheets';
import { Element, Component, h, JSX, Prop, State, Watch, Host } from '@stencil/core';
import { KernKopfzeileLanguage, OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { watchString } from '../../utils/prop.validators';

@Component({
	tag: 'kern-kopfzeile',
	shadow: true,
	styleUrls: {
		default: './style.css',
	},
})

/*
 * @name: KernKopfzeile
 * @description: Diese Komponente darf nur von öffentlichen Stellen der Bundesrepublik Deutschland verwendet werden.
 */
export class KernKopfzeile implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	@Element() hostElement!: HTMLElement;

	/**
	 * Gibt an, welche Sprache verwendet werden soll.
	 */
	@Prop() public _language: KernKopfzeileLanguage = 'german';

	/**
	 * Gibt den Text der Überschrift an.
	 */

	@Prop() public _label?: string;

	/**
	 * Setzt die Breite der Kopfzeile auf 100%.
	 */
	@Prop() public _fluid?: boolean = false;

	@State() public state: States = {
		_language: 'german',
		_fluid: false,
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		// watchString(this, '_label', value);
		watchString(this, '_label', value, {
			defaultValue: undefined,
		});
	}

	@Watch('_language')
	public validateLanguage(value?: KernKopfzeileLanguage): void {
		if (typeof value === 'string') {
			this.state._language = value;
		}
	}

	@Watch('_fluid')
	public validateFluid(value?: boolean): void {
		if (typeof value === 'boolean') {
			this.state._fluid = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLanguage(this._language);
		this.validateLabel(this._label);
		this.validateFluid(this._fluid);
	}

	public render(): JSX.Element {
		let labelText = this.state._label;
		const germanLabel = 'Eine offizielle Website der Bundesrepublik Deutschland';
		const englishLabel = 'An official website of the Federal Republic of Germany';
		labelText = this._label ? this._label : this.state._language === 'german' ? germanLabel : englishLabel;

		return (
			<Host>
				<div class="kopfzeile">
					<div class={this._fluid ? 'container-fluid' : 'container'}>
						<div class="kopfzeile__content">
							<span class="kopfzeile__flagge">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 16">
									<path fill="#000" d="M0 .5h24v5.333H0z" />
									<path fill="red" d="M0 5.833h24v5.333H0z" />
									<path fill="#FACA2C" d="M0 11.167h24V16.5H0z" />
								</svg>
							</span>
							<span class="kopfzeile__label">{labelText}</span>
						</div>
					</div>
				</div>
				<div class="kopfzeile__panel">
					<slot />
				</div>
			</Host>
		);
	}
}
