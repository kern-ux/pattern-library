import { Generic, patchTheme, patchThemeTag } from 'adopted-style-sheets';
import { querySelectorAll } from 'query-selector-all-shadow-root';
import { querySelector } from 'query-selector-shadow-root';

type SetStateHooks = {
	afterPatch?: Generic.Element.StateHooksCallback;
	beforePatch?: Generic.Element.NextStateHooksCallback;
};

const patchState = (component: Generic.Element.Component): void => {
	component.nextHooks?.forEach((hooks, key) => {
		const beforePatch = hooks.get('beforePatch') as Generic.Element.NextStateHooksCallback;
		if (typeof beforePatch === 'function') {
			beforePatch(component.nextState?.get(key), component.nextState as Map<string, unknown>, component, key);
		}
	});
	/**
	 * Wenn in beforePatch Methoden die Änderung verworfen wird,
	 * muss auch nicht der State aktualisiert und neu gerendert
	 * werden.
	 */
	if ((component.nextState as Map<string, unknown>)?.size > 0) {
		component.state = {
			...component.state,
			...Object.fromEntries(component.nextState as Map<string, unknown>),
		};
		delete component.nextState;

		component.nextHooks?.forEach((hooks, key) => {
			const afterPatch = hooks.get('afterPatch') as Generic.Element.StateHooksCallback;
			if (typeof afterPatch === 'function') {
				afterPatch(component.state[key], component.state, component, key);
			}
		});
	}
	delete component.nextHooks;
};

export const setState = <T>(component: Generic.Element.Component, propName: string, value?: T | null | undefined, hooks: SetStateHooks = {}): void => {
	if (component.nextHooks === undefined) {
		component.nextHooks = new Map();
	}
	if (component.nextState === undefined) {
		component.nextState = new Map();
	}
	if (component.nextState.get(propName) !== value) {
		const nextHooks = component.nextHooks.get(propName);
		if (nextHooks instanceof Map === false) {
			component.nextHooks.set(propName, new Map());
		}
		if (typeof hooks.afterPatch === 'function') {
			component.nextHooks.get(propName)?.set('afterPatch', hooks.afterPatch);
		}
		if (typeof hooks.beforePatch === 'function') {
			component.nextHooks.get(propName)?.set('beforePatch', hooks.beforePatch);
		}
		component.nextState.set(propName, value);
		patchState(component);
	}
};

type WatchOptions = {
	defaultValue?: unknown;
	hooks?: SetStateHooks;
	required?: boolean;
};

export type WatchBooleanOptions = WatchOptions & {
	defaultValue?: boolean | null;
};

export type WatchStringOptions = WatchOptions & {
	defaultValue?: string | null;
	maxLength?: number;
	minLength?: number;
};

export type WatchNumberOptions = WatchOptions & {
	defaultValue?: number | null;
	min?: number;
	max?: number;
};

export function watchValidator<T>(
	component: Generic.Element.Component,
	propName: string,
	validationFunction: (value?: T) => boolean,
	requiredGeneric: Set<string | null | undefined>,
	value?: T,
	options: WatchOptions = {},
): void {
	if (validationFunction(value)) {
		/**
		 * Triff zu, wenn der Wert VALIDE ist.
		 */
		setState(component, propName, value, options.hooks);
	} else if ((value === undefined || value === null) && !options.required) {
		/**
		 * Triff zu, wenn der Wert entweder ...
		 * - UNDEFINED oder NULL
		 * - und NICHT REQUIRED
		 * ... ist.
		 */
		setState(component, propName, options.defaultValue, options.hooks);
	} else {
		/**
		 * Triff zu, wenn der Wert NICHT valide ist.
		 */
		if (!options.required) {
			requiredGeneric.add(null);
		}
	}
}

export const watchString = (component: Generic.Element.Component, propName: string, value?: string, options: WatchStringOptions = {}): void => {
	const minLength = typeof options.minLength === 'number' ? options?.minLength : 0;
	watchValidator(
		component,
		propName,
		(value): boolean =>
			typeof value === 'string' && value.length >= minLength && (typeof options?.maxLength === 'undefined' || value.length <= options.maxLength),
		new Set([`String`]),
		value,
		options,
	);
};

export const kernQuerySelector = <T extends Element>(selector: string, node?: Document | HTMLElement | ShadowRoot): T | null =>
	querySelector<T>(selector, node);

export const kernQuerySelectorAll = <T extends Element>(selector: string, node?: Document | HTMLElement | ShadowRoot): T[] =>
	querySelectorAll<T>(selector, node);

export function showExpertSlot(slot: string): boolean {
	// Check if the slot is filled
	return slot !== undefined && slot !== null && slot.trim() !== '';
}

export class KernDevHelper {
	public static readonly patchTheme = patchTheme;
	public static readonly patchThemeTag = patchThemeTag;
	public static readonly querySelector = kernQuerySelector;
	public static readonly querySelectorAll = kernQuerySelectorAll;
}
