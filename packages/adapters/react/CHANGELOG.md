# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.5.14](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.13...v1.5.14) (2024-11-20)

**Note:** Version bump only for package @kern-ux/react

## [1.5.13](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.12...v1.5.13) (2024-11-15)

**Note:** Version bump only for package @kern-ux/react

## [1.5.12](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.11...v1.5.12) (2024-11-08)

**Note:** Version bump only for package @kern-ux/react

## [1.5.11](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.10...v1.5.11) (2024-10-14)

**Note:** Version bump only for package @kern-ux/react

## [1.5.10](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.9...v1.5.10) (2024-10-14)

**Note:** Version bump only for package @kern-ux/react

## [1.5.9](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.8...v1.5.9) (2024-10-14)

**Note:** Version bump only for package @kern-ux/react

## [1.5.8](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.7...v1.5.8) (2024-10-14)

**Note:** Version bump only for package @kern-ux/react

## [1.5.7](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.6...v1.5.7) (2024-10-14)

**Note:** Version bump only for package @kern-ux/react

## [1.5.6](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.5...v1.5.6) (2024-10-11)

**Note:** Version bump only for package @kern-ux/react

## [1.5.5](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.4...v1.5.5) (2024-10-10)

**Note:** Version bump only for package @kern-ux/react

## [1.5.4](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.3...v1.5.4) (2024-10-10)

**Note:** Version bump only for package @kern-ux/react

## [1.5.3](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.2...v1.5.3) (2024-10-10)

**Note:** Version bump only for package @kern-ux/react

## [1.5.2](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.1...v1.5.2) (2024-10-10)

**Note:** Version bump only for package @kern-ux/react

## [1.5.1](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.0...v1.5.1) (2024-10-10)

**Note:** Version bump only for package @kern-ux/react

# [1.5.0](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.4.0...v1.5.0) (2024-10-10)

**Note:** Version bump only for package @kern-ux/react

# [1.4.0](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.3.0...v1.4.0) (2024-10-02)

**Note:** Version bump only for package @kern-ux/react

# [1.3.0](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.21...v1.3.0) (2024-10-02)

**Note:** Version bump only for package @kern-ux/react

## [1.2.21](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.20...v1.2.21) (2024-09-26)

**Note:** Version bump only for package @kern-ux/react

## [1.2.20](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.19...v1.2.20) (2024-09-26)

**Note:** Version bump only for package @kern-ux/react

## [1.2.19](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.18...v1.2.19) (2024-09-26)

**Note:** Version bump only for package @kern-ux/react

## [1.2.18](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.17...v1.2.18) (2024-09-26)

**Note:** Version bump only for package @kern-ux/react
