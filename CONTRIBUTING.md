# Contributing

Wir freuen uns sehr, wenn du zu **KERN-UX** beiträgst und dabei hilfst, das Projekt weiter zu verbessern! Als Mitwirkender bitten wir dich, die folgenden Richtlinien zu beachten:

- [Contributing](#contributing)
- [Code of Conduct](#code-of-conduct)
- [Fragen und Probleme](#fragen-und-probleme)
- [Fehler melden](#fehler-melden)
- [Weiterentwicklung](#weiterentwicklung)

## Code of Conduct

Hilf uns, **KERN-UX** offen und inklusiv zu halten. Bitte lies und befolge unseren [Code of Conduct](CODE_OF_CONDUCT.md).

## Fragen und Probleme

Bitte erstelle keine Issue-Tickets für allgemeine Supportfragen. Wir möchten das Ticketsystem ausschließlich für die Nachverfolgung von Fehlerberichten und Funktionsanfragen verwenden.

Wenn du direkt mit uns in Kontakt treten möchtest, schreibe uns eine E-Mail an [hallo@kern-ux.de](mailto:hallo@kern-ux.de) oder nutze unseren [Messenger](https://chat.kern-ux.de).

## Fehler melden

Falls du einen Fehler im Quellcode entdeckst, melde diesen bitte über unser [Ticketsystem](https://gitlab.opencode.de/kern-ux/pattern-library/-/issues/new).

Noch besser wäre es, wenn du uns eine Lösung als Pull Request vorschlägst.

## Weiterentwicklung

Du kannst neue Funktionen anfordern, indem du ein Ticket in unserem [Ticketsystem](https://gitlab.opencode.de/kern-ux/pattern-library/-/issues/new) erstellst. Wenn du selbst an der Implementierung einer Funktion arbeiten möchtest, beachte bitte die folgenden Schritte:

- Bei größeren Neuerungen ist es ratsam, vor der Implementierung ein Ticket mit einer Beschreibung der neuen Funktion zu erstellen.
- Für kleinere Änderungen kannst du direkt einen Pull Request mit einer kurzen Begründung einreichen.

Vielen Dank für deinen Beitrag!
